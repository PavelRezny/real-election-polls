import code.Poll;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class PollTest {

    public Poll createPoll(){
        Poll p = new Poll();
        p.setPollster("Super Poll");
        p.setDate(LocalDateTime.of(2020, 10, 5, 20, 20));
        p.getResults().add("First", 45);
        p.getResults().add("Second", 48);
        p.getResults().add("Third", 2);
        return p;
    }

    @Test
    public void testGetUndecided(){
        Poll p = createPoll();
        assertEquals(5, p.getResults().getUndecided());
    }

    @Test
    public void testGetNet(){
        Poll p = createPoll();
        assertEquals(-3, p.getResults().getNet());
    }

    @Test
    public void testBestOfRest1(){
        Poll p = createPoll();
        assertEquals("Third " + "2", p.getResults().getBestOfRest());
    }

    @Test
    public void testBestOfRest2(){
        Poll p = new Poll();
        p.getResults().add("First", 50);
        p.getResults().add("Second", 40);
        assertEquals("", p.getResults().getBestOfRest());
    }

    @Test
    public void testResultToString(){
        Poll p = createPoll();
        assertEquals("First;45;Second;48;Third;2;", p.getResults().toString());
    }

}