import code.Filters;
import code.Poll;
import code.State;
import code.Type;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FiltersTest {
/*
    @Test public void testType(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "State", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.SENATE, "Type", 15, "State", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name2", Type.SENATE, "Type", 15, "State", "URL")));
        List<Poll> result = Filters.filterType(polls, Type.SENATE);
        assertEquals(2, result.size());
        assertEquals("Name", result.get(0).getPollster());
        assertEquals("Name2", result.get(1).getPollster());
        result = Filters.filterType(polls, Type.SHOW_ALL);
        assertEquals(3, result.size());
    }

    @Test public void testPollster(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "State", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.SENATE, "Type", 15, "State", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name2", Type.SENATE, "Type", 15, "State", "URL")));
        List<Poll> result = Filters.filterPollster(polls, "Name");
        assertEquals(2, result.size());
        assertEquals("Name", result.get(0).getPollster());
        assertEquals("Name", result.get(1).getPollster());
        result = Filters.filterPollster(polls, "Name2");
        assertEquals(1, result.size());
        assertEquals("Name2", result.get(0).getPollster());
        result = Filters.filterPollster(polls, "Show All");
        assertEquals(3, result.size());
    }

    @Test public void testState(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.SENATE, "Type", 15, "Ala.", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.SENATE, "Type", 15, "Ark.", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name2", Type.SENATE, "Type", 15, "Ala.", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name3", Type.SENATE, "Type", 15, "", "URL")));
        List<Poll> result = Filters.filterState(polls, State.ALABAMA, Type.SENATE);
        assertEquals(2, result.size());
        assertEquals("Ala.", result.get(0).getState());
        assertEquals("Ala.", result.get(1).getState());
        result = Filters.filterState(polls, State.ARKANSAS, Type.SENATE);
        assertEquals(1, result.size());
        assertEquals("Ark.", result.get(0).getState());
        result = Filters.filterState(polls, State.SHOW_ALL, Type.SENATE);
        assertEquals(4, result.size());
        result = Filters.filterState(polls, State.NATIONWIDE, Type.SENATE);
        assertEquals(1, result.size());
        assertEquals("", result.get(0).getState());
        assertEquals("Name3", result.get(0).getPollster());
    }

    @Test public void testState2(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-6", "URL"),
                new Poll(LocalDateTime.of(1,1,1, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-6", "URL")));
        List<Poll> result = Filters.filterState(polls, State.ARIZONA, Type.HOUSE);
        assertEquals(2, result.size());
        assertEquals("6", result.get(0).getDistrict());
        assertEquals("6", result.get(1).getDistrict());
        assertEquals("AZ", result.get(0).removeDistrictNumber());
        assertEquals("AZ", result.get(1).removeDistrictNumber());
    }

    @Test public void testDate(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(2020,6,2, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "Ala.", "URL"),
                new Poll(LocalDateTime.of(2020,7,15, 0, 0), "1.1.1", "Name", Type.SENATE, "Type", 15, "Ark.", "URL"),
                new Poll(LocalDateTime.of(2020,7,20, 0, 0), "1.1.1", "Name2", Type.SENATE, "Type", 15, "Ala.", "URL"),
                new Poll(LocalDateTime.of(2020,10,1, 0, 0), "1.1.1", "Name3", Type.SENATE, "Type", 15, "", "URL")));
        Collections.sort(polls);
        List<Poll> result = Filters.filterDate(polls, LocalDate.of(2019, 1, 1), LocalDate.of(2021, 1, 1));
        assertEquals(4, result.size());
        result = Filters.filterDate(polls, LocalDate.of(2020, 7, 15), LocalDate.of(2020, 12, 31));
        assertEquals(2, result.size());
        result = Filters.filterDate(polls, LocalDate.of(2020, 7, 20), LocalDate.of(2020, 10, 1));
        assertEquals(1, result.size());
        result = Filters.filterDate(polls, LocalDate.of(2020, 7, 20), LocalDate.of(2020, 9, 1));
        assertEquals(0, result.size());
    }

    @Test public void testDistrict(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(2020,6,2, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-1", "URL"),
                new Poll(LocalDateTime.of(2020,7,15, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-1", "URL"),
                new Poll(LocalDateTime.of(2020,7,20, 0, 0), "1.1.1", "Name2", Type.HOUSE, "Type", 15, "AZ-2", "URL"),
                new Poll(LocalDateTime.of(2020,10,1, 0 ,0), "1.1.1", "Name3", Type.HOUSE, "Type", 15, "Ala.", "URL")));
        List<Poll> result = Filters.filterDistrict(polls, "1", false);
        assertEquals(2, result.size());
        result = Filters.filterDistrict(polls, "Show All", false);
        assertEquals(4, result.size());
        result = Filters.filterDistrict(polls, "2", false);
        assertEquals(1, result.size());
        result = Filters.filterDistrict(polls, "3", false);
        assertEquals(0, result.size());
    }

    @Test public void testLowestUndecided(){
        List<Poll> polls = new ArrayList<>(Arrays.asList(new Poll(LocalDateTime.of(2020,6,2, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-1", "URL"),
                new Poll(LocalDateTime.of(2020,7,15, 0, 0), "1.1.1", "Name", Type.HOUSE, "Type", 15, "AZ-1", "URL")));
        polls.get(0).getResults().add("A", 50);
        polls.get(0).getResults().add("B", 40);
        polls.get(1).getResults().add("A", 50);
        polls.get(1).getResults().add("B", 45);
        assertEquals(polls.get(1), Filters.getLowestUndecided(polls));
    }
*/
}