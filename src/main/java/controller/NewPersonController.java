package controller;

import code.Party;
import code.Person;
import data.PersonDao;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewPersonController {

    @FXML private Button bCreate;
    @FXML private ComboBox<Party> comParty;
    @FXML private TextField tfName;

    private PersonDao personDao;

    public void start(PersonDao personDao){
        this.personDao = personDao;

        comboBoxSetUp();
    }

    private void comboBoxSetUp(){
        ObservableList<Party> olParty = FXCollections.observableArrayList(Party.values());
        comParty.setItems(olParty);
    }

    public void bCreateOnAction(){
        if(comParty.getValue() != null && !tfName.getText().isBlank()){
            Person p = new Person(tfName.getText(), comParty.getValue());
            personDao.create(p);

            ((Stage)comParty.getScene().getWindow()).close();
        }
    }

}
