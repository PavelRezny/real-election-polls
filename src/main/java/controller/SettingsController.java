package controller;

import code.*;
import data.*;
import javafx.beans.property.SimpleStringProperty;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import manager.WindowManager;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class SettingsController {

    @FXML private Button bAdd, bRemove, bAddDistrict, bAddResult, bAdd1, bRemove1;
    @FXML private TextField tfName;
    @FXML private DatePicker dpFrom, dpTo, dpFrom1, dpTo1;
    @FXML private TableView<TimePeriod> tvList;
    @FXML private TableColumn<TimePeriod, String> cFrom, cTo, cName;
    @FXML private TableView<TimePeriod> tvList1;
    @FXML private TableColumn<TimePeriod, String> cFrom1, cTo1;
    @FXML private TableView<State> tvStates;
    @FXML private TableColumn<State, String> cStateName;
    @FXML private TableView<District> tvDistricts, tvSenateDistricts, tvPresidentDistrict, tvGovernorDistrict;
    @FXML private TableColumn<District, String> cDistrictName, cSenateDistrictName, cPresidentDistrictName, cGovernorDistrictName;
    @FXML private TableView<ElectionResult> tvYears;
    @FXML private TableColumn<ElectionResult, String> cYear;
    @FXML private TableView<AddResultController.Result> tvResults;
    @FXML private TableColumn<AddResultController.Result, String> cCandidateName, cVotes, cPercent;

    private TimePeriodDao timePeriodDao;
    private DistrictDao districtDao;
    private PersonDao personDao;
    private ElectionResultDao electionResultDao;
    private ResultNumberDao resultNumberDao;
    private ResultPersonDao resultPersonDao;

    private District selectedDistrict;

    public void start(TimePeriodDao timePeriodDao, DistrictDao districtDao, PersonDao personDao, ElectionResultDao electionResultDao, ResultNumberDao resultNumberDao, ResultPersonDao resultPersonDao){
        this.timePeriodDao = timePeriodDao;
        this.districtDao = districtDao;
        this.personDao = personDao;
        this.resultNumberDao = resultNumberDao;
        this.electionResultDao = electionResultDao;
        this.resultPersonDao = resultPersonDao;

        timePeriodTableSetUp();
        fillTimePeriodTable();
        statesTableSetUp();
        fillStatesTable();
        districtsTableSetUp();
        fillDistrictsTable(null);
        senateDistrictsTableSetUp();
        fillSenateDistrictsTable(null);
        presidentDistrictsTableSetUp();
        fillPresidentDistrictsTable(null);
        governorDistrictsTableSetUp();
        fillGovernorDistrictsTable(null);
        yearsTableSetUp();
        fillYearsTable(null);
        resultsTableSetUp();
        fillResultsTable(null);
    }

    @SuppressWarnings("unchecked")
    public void clicked(Event e){
        selectedDistrict = ((TableView<District>)(e.getSource())).getSelectionModel().getSelectedItem();
        fillYearsTable(selectedDistrict);
        fillResultsTable(null);
    }

    private void fillPresidentDistrictsTable(State state){
        tvPresidentDistrict.getItems().clear();
        if(state != null)
            tvPresidentDistrict.getItems().add(state.getPresidentialDistrict());
        tvPresidentDistrict.refresh();
    }

    private void fillGovernorDistrictsTable(State state){
        tvGovernorDistrict.getItems().clear();
        if(state != null)
            tvGovernorDistrict.getItems().add(state.getGubernialDistrict());
        tvGovernorDistrict.refresh();
    }

    private void presidentDistrictsTableSetUp(){
        cPresidentDistrictName.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getNumber())));
        cPresidentDistrictName.setReorderable(false);
    }

    private void governorDistrictsTableSetUp(){
        cGovernorDistrictName.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getNumber())));
        cGovernorDistrictName.setReorderable(false);
    }

    public void bAddResultOnAction(){
        ((AddResultController)(Objects.requireNonNull(WindowManager.openWindow("AddResult", 600, 500, true))))
                .start(selectedDistrict, personDao, resultPersonDao, resultNumberDao, electionResultDao);
    }

    public void tvYearsOnAction(){
        if(tvYears.getSelectionModel().getSelectedItem() != null){
            ElectionResult selectedElectionResult = tvYears.getSelectionModel().getSelectedItem();
            fillResultsTable(selectedElectionResult);
        }
    }

    private void fillResultsTable(ElectionResult electionResult){
        List<AddResultController.Result> list = new ArrayList<>();
        if(electionResult != null){
            int totalVotes = 0;
            for(int x = 0; x < electionResult.getNames().size(); x++){
                list.add(new AddResultController.Result(electionResult.getNames().get(x).getPerson(), electionResult.getNumbers().get(x).getVotes()));
                totalVotes += list.get(list.size() - 1).getVotes();
            }

            for(AddResultController.Result r : list){
                double percent = (r.getVotes() / (double)totalVotes) * 100;
                r.setPercent(percent);
            }
        }

        tvResults.getItems().clear();
        tvResults.getItems().addAll(list);
        tvResults.refresh();
    }

    private void resultsTableSetUp(){
        cCandidateName.setCellValueFactory(item -> new SimpleStringProperty(item.getValue().getPerson().getName() + " (" + item.getValue().getPerson().getParty().getLetter() + ")"));
        cCandidateName.setReorderable(false);
        cVotes.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getVotes())));
        cVotes.setReorderable(false);
        cPercent.setCellValueFactory(item -> new SimpleStringProperty(Double.toString(item.getValue().getPercent()) + "%"));
        cPercent.setReorderable(false);
    }

    private void fillYearsTable(District district){
        tvYears.getItems().clear();
        if(district != null)
            tvYears.getItems().addAll(district.getResult());
        tvYears.refresh();
    }

    private void yearsTableSetUp(){
        cYear.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getDate().getYear())));
        cYear.setReorderable(false);
    }

    private void senateDistrictsTableSetUp(){
        cSenateDistrictName.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getNumber())));
        cSenateDistrictName.setReorderable(false);
    }

    private void fillSenateDistrictsTable(State state){
        tvSenateDistricts.getItems().clear();
        if(state != null)
            tvSenateDistricts.getItems().addAll(state.getSenateDistricts());
        tvSenateDistricts.refresh();
    }

    public void tvStatesOnAction(){
        if(tvStates.getSelectionModel().getSelectedItem() != null) {
            State state = tvStates.getSelectionModel().getSelectedItem();
            fillDistrictsTable(state);
            fillSenateDistrictsTable(state);
            fillGovernorDistrictsTable(state);
            fillPresidentDistrictsTable(state);
        }
    }

    private void fillDistrictsTable(State state){
        tvDistricts.getItems().clear();
        if(state != null)
            tvDistricts.getItems().addAll(state.getHouseDistricts());
        tvDistricts.refresh();
    }

    private void districtsTableSetUp(){
        cDistrictName.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getNumber())));
        cDistrictName.setReorderable(false);
    }

    private void fillStatesTable(){
        tvStates.getItems().clear();
        tvStates.getItems().addAll(MainController.states);
        tvStates.refresh();
    }

    private void statesTableSetUp(){
        cStateName.setCellValueFactory(item -> new SimpleStringProperty(item.getValue().getFullName()));
        cStateName.setReorderable(false);
    }

    private void timePeriodTableSetUp(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LL/yyyy");

        cFrom.setCellValueFactory(item -> new SimpleStringProperty(formatter.format(item.getValue().getFrom())));
        cFrom.setReorderable(false);
        cTo.setCellValueFactory(item -> new SimpleStringProperty(formatter.format(item.getValue().getTo())));
        cTo.setReorderable(false);
        cName.setCellValueFactory(item -> new SimpleStringProperty(item.getValue().getName()));
        cName.setReorderable(false);

        cFrom1.setCellValueFactory(item -> new SimpleStringProperty(formatter.format(item.getValue().getFrom())));
        cFrom1.setReorderable(false);
        cTo1.setCellValueFactory(item -> new SimpleStringProperty(formatter.format(item.getValue().getTo())));
        cTo1.setReorderable(false);
    }

    private void fillTimePeriodTable(){
        tvList.getItems().clear();
        tvList.getItems().addAll(timePeriodDao.findAll(false));
        tvList.refresh();

        tvList1.getItems().clear();
        tvList1.getItems().addAll(timePeriodDao.findAll(true));
        tvList1.refresh();
    }

    public void bAddOnAction(){
        if(!tfName.getText().isBlank() && dpFrom.getValue() != null && dpTo.getValue() != null){
            TimePeriod a = new TimePeriod(dpFrom.getValue().atStartOfDay(), dpTo.getValue().atStartOfDay(), tfName.getText());
            timePeriodDao.create(a);

            fillTimePeriodTable();

            tfName.clear();
            dpFrom.setValue(null);
            dpTo.setValue(null);
        }
    }

    public void bRemoveOnAction(){
        if(tvList.getSelectionModel().getSelectedItem() != null){
            timePeriodDao.delete(tvList.getSelectionModel().getSelectedItem());
            fillTimePeriodTable();
        }
    }

    public void bAdd1OnAction(){
        if(dpFrom1.getValue() != null && dpTo1.getValue() != null){
            TimePeriod a = new TimePeriod(dpFrom1.getValue().atStartOfDay(), dpTo1.getValue().atStartOfDay(), "");
            timePeriodDao.create(a);

            fillTimePeriodTable();

            dpFrom1.setValue(null);
            dpTo1.setValue(null);
        }
    }

    public void bRemove1OnAction(){
        if(tvList1.getSelectionModel().getSelectedItem() != null){
            timePeriodDao.delete(tvList1.getSelectionModel().getSelectedItem());
            fillTimePeriodTable();
        }
    }

    public void bAddDistrictOnAction(){
        if(tvStates.getSelectionModel().getSelectedItem() != null){
            District d = new District(1000 + tvStates.getSelectionModel().getSelectedItem().getHouseDistricts().size() + 1);
            d.setStateID(tvStates.getSelectionModel().getSelectedItem().getID());
            tvStates.getSelectionModel().getSelectedItem().getHouseDistricts().add(d);
            districtDao.create(d);
            fillDistrictsTable(tvStates.getSelectionModel().getSelectedItem());
        }
    }

}
