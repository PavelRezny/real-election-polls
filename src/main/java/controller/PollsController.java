package controller;

import code.*;
import data.PollDao;
import data.TimePeriodDao;
import javafx.application.HostServices;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.*;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.Text;
import manager.ChartManager;
import manager.WindowManager;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

public class PollsController {

    @FXML private TableView<Poll> tvPolls;
    @FXML private TableColumn<Poll, String> cDate, cPollster, cSize, cType, cLeft, cLeftNum, cRightNum, cRight, cState, cDistrict, cNet, cBestOfRest, cUndecided, cPollType;
    @FXML private DatePicker dpFrom, dpTo;
    @FXML private ComboBox<String> comDistrict, comPollster;
    @FXML private ComboBox<Type> comType;
    @FXML private ComboBox<State> comStates;
    @FXML private ComboBox<ElectionResult> com;
    @FXML private Text tAverage, tNet;
    @FXML private LineChart<String, Number> lcChart;
    @FXML private CategoryAxis caAxis;
    @FXML private NumberAxis naAxis;

    private PollDao pollDao;
    private TimePeriodDao timePeriodDao;

    private ObservableList<Poll> olPolls;

    private ContextMenu menu;
    private MenuItem miBanPoll, miBanPollster, miDetails, miDelete;

    private ChartManager chartManager;

    private HostServices hostServices;

    //TODO: Add an options for using corrected polls

    public void start(PollDao pollDao, HostServices hostServices, TimePeriodDao timePeriodDao){
        this.pollDao = pollDao;
        this.hostServices = hostServices;
        this.timePeriodDao = timePeriodDao;

        DownloadData.getContent(pollDao);

        loadPolls();
        setTable();
        setCurrentDates();
        setContextMenu();
        setNumOfDistricts(53);
        setComboBoxes();
        mouseHandler();
        setVisibility();

        tvPolls.setItems(olPolls);
        tableStyle();
        tvPolls.refresh();

        chartManager = new ChartManager(lcChart, caAxis, naAxis);
    }

    public void loadPolls(){
        Task<List<Poll>> task = new Task<>() {
            @Override
            public List<Poll> call() {
                return pollDao.find(Collections.singletonList(null));
            }
        };

        new Thread(task).start();

        try{
            olPolls = FXCollections.observableArrayList(task.get());
        }catch(ExecutionException | InterruptedException e){
            e.printStackTrace();
        }
    }

    // FIXME: The table header is broken
    public void tableStyle(){
        tvPolls.setRowFactory(tv -> new TableRow<>() {
            @Override
            public void updateItem(Poll poll, boolean empty) {
                super.updateItem(poll, empty);
                if(!empty){
                    if(isSelected()){
                        setBackground(new Background(new BackgroundFill(((Color) getBackground().getFills().get(0).getFill()).darker(), CornerRadii.EMPTY, Insets.EMPTY)));
                    }else{
                        if(poll.isBanned()) {
                            setBackground(new Background(new BackgroundFill(Color.valueOf("#FFD2D2"), CornerRadii.EMPTY, Insets.EMPTY)));
                        }else if(poll.isHighlight()){
                            setBackground(new Background(new BackgroundFill(Color.valueOf("#D3D3D3"), CornerRadii.EMPTY, Insets.EMPTY)));
                        }else{
                            setBackground(new Background(new BackgroundFill(Color.valueOf("#FFFFFF"), CornerRadii.EMPTY, Insets.EMPTY)));
                        }
                        if(poll.is_new()){
                            setStyle("-fx-font-weight: bold;");
                        }
                    }
                    setBorder(new Border(new BorderStroke(Color.GREY, BorderStrokeStyle.SOLID, CornerRadii.EMPTY, new BorderWidths(0.2))));
                }
            }
        });
    }

    public void mouseHandler(){
        tvPolls.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            if(e.getButton() == MouseButton.SECONDARY){
                if(tvPolls.getSelectionModel().getSelectedItem().isCustom()){
                    if(!menu.getItems().contains(miDelete))
                        menu.getItems().add(miDelete);
                }else{
                    menu.getItems().remove(miDelete);
                }
                menu.show(tvPolls, e.getScreenX(), e.getScreenY());
            }else{
                menu.hide();
            }
        });

        miDelete.setOnAction(e -> {
            Poll p = tvPolls.getSelectionModel().getSelectedItem();
            if(p != null){
                pollDao.delete(p);
                loadPolls();
                tvPolls.setItems(olPolls);
            }
        });

        miDetails.setOnAction(e -> {
            Poll p = tvPolls.getSelectionModel().getSelectedItem();
            if(p != null && !p.getUrl().isEmpty()){
                hostServices.showDocument(p.getUrl());
            }
        });

        miBanPoll.setOnAction(e -> {
            Poll poll = tvPolls.getSelectionModel().getSelectedItem();
            poll.setBanned(true);

            Task<Void> task = new Task<>() {
                @Override
                public Void call() {
                    pollDao.update(poll);
                    return null;
                }
            };

            new Thread(task).start();

            tvPolls.refresh();
            menu.hide();
        });

        miBanPollster.setOnAction(e -> {
            List<Poll> toBan = new ArrayList<>();
            for(Poll p : olPolls){
                if(p.getPollster().equals(tvPolls.getSelectionModel().getSelectedItem().getPollster())){
                    p.setBanned(true);
                    toBan.add(p);
                }
            }

            Task<Void> task = new Task<>() {
                @Override
                public Void call() {
                    for(Poll p : toBan){
                        pollDao.update(p);
                    }
                    return null;
                }
            };

            new Thread(task).start();

            tvPolls.refresh();
            menu.hide();
        });
    }

    public void setComboBoxes(){
        ObservableList<Type> olTypes = FXCollections.observableArrayList(Type.values());
        comType.setItems(olTypes);
        comType.getSelectionModel().select(0);

        ObservableList<State> olStates = FXCollections.observableArrayList(MainController.states);
        olStates.add(0, MainController.SHOW_ALL);
        // FIXME: Will fail
        //olStates.add(1, MainController.NATIONWIDE);
        comStates.setItems(olStates);
        comStates.getSelectionModel().select(0);

        ObservableList<String> olPollsters = FXCollections.observableArrayList();
        for(Poll p : pollDao.find(Collections.singletonList(null))){
            if(!olPollsters.contains(p.getPollster())){
                olPollsters.add(p.getPollster());
            }
        }

        Collections.sort(olPollsters);
        olPollsters.set(0, "Show All");
        comPollster.setItems(olPollsters);
        comPollster.getSelectionModel().select(0);
    }

    public void setContextMenu(){
        menu = new ContextMenu();
        miBanPoll = new MenuItem("Ban Poll");
        miBanPollster = new MenuItem("Ban Pollster");
        miDetails = new MenuItem("Details");
        miDelete = new MenuItem("Delete");
        menu.getItems().addAll(miBanPoll, miBanPollster, miDetails, miDelete);
    }

    // TODO: Display pollster rating
    // TODO: Make a better way to display more than two candidates

    public void setTable(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LL/yyyy");

        cDate.setCellValueFactory(poll -> new SimpleStringProperty(formatter.format(poll.getValue().getDate())));
        cDate.setReorderable(false);

        cPollster.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getPollster()));
        cPollster.setReorderable(false);

        cSize.setCellValueFactory(poll -> new SimpleStringProperty(Integer.toString(poll.getValue().getSampleSize())));
        cSize.setReorderable(false);

        cType.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getSampleType()));
        cType.setReorderable(false);

        cLeft.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getResults().getNames().get(0)));
        cLeft.setReorderable(false);

        cLeftNum.setCellValueFactory(poll -> new SimpleStringProperty(Integer.toString(poll.getValue().getResults().getNumbers().get(0))));
        cLeftNum.setReorderable(false);

        cRight.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getResults().getNames().get(1)));
        cRight.setReorderable(false);

        cRightNum.setCellValueFactory(poll -> new SimpleStringProperty(Integer.toString(poll.getValue().getResults().getNumbers().get(1))));
        cRightNum.setReorderable(false);

        cState.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getState()));
        cState.setReorderable(false);

        cDistrict.setCellValueFactory(poll -> new SimpleStringProperty(((poll.getValue().getDistrict() == 0) ? "" : Integer.toString(poll.getValue().getDistrict()))));
        cDistrict.setReorderable(false);

        cNet.setCellValueFactory(poll -> new SimpleStringProperty((poll.getValue().getResults().getNet() > 0) ? "+" + poll.getValue().getResults().getNet() : Integer.toString(poll.getValue().getResults().getNet())));
        cNet.setReorderable(false);

        cUndecided.setCellValueFactory(poll -> new SimpleStringProperty(Integer.toString(poll.getValue().getResults().getUndecided())));
        cUndecided.setReorderable(false);

        cBestOfRest.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getResults().getBestOfRest()));
        cBestOfRest.setReorderable(false);

        cPollType.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getPollType().getFormattedText()));
        cPollType.setReorderable(false);
    }

    public void updateTable(List<Poll> polls){
        olPolls = FXCollections.observableArrayList(polls);
        tvPolls.setItems(olPolls);
        tvPolls.refresh();
    }

    public void bShowAllOnAction(){
        List<Poll> allPolls = pollDao.find(Collections.singletonList(null));

        comPollster.getSelectionModel().select(0);
        comDistrict.getSelectionModel().select(0);
        comStates.getSelectionModel().select(0);
        comType.getSelectionModel().select(0);
        com.getItems().clear();
        olPolls = FXCollections.observableArrayList(allPolls);

        for(Poll p : allPolls){
            p.setHighlight(false);
        }

        setCurrentDates();
        lcChart.getData().clear();
        tAverage.setText("");
        tvPolls.setItems(olPolls);
        tvPolls.refresh();
        setVisibility();
    }

    public void setCurrentDates(){
        dpFrom.setValue(olPolls.get(olPolls.size() - 1).getDate().toLocalDate());
        dpTo.setValue(olPolls.get(0).getDate().toLocalDate());
    }

    public void setNumOfDistricts(int num){
        ObservableList<String> olDistricts = FXCollections.observableArrayList("Show All");
        olDistricts.addAll(IntStream.rangeClosed(1, num).boxed().collect(Collectors.toList()).stream().map(String::valueOf).collect(Collectors.toList()));
        comDistrict.getItems().clear();
        comDistrict.setItems(olDistricts);
        comDistrict.getSelectionModel().select(0);
    }

    public void comcom(){
        if(com.getItems().size() > 0){
            if(com.getSelectionModel().getSelectedIndex() == 0){
                dpFrom.setValue(LocalDate.of(2020,6,26));
                dpTo.setValue(com.getValue().getDate());
            }else if(com.getSelectionModel().getSelectedIndex() == com.getItems().size()){
                dpFrom.setValue(com.getValue().getDate());
                dpTo.setValue(dpFrom.getValue().plusYears(4));
            }else{
                dpFrom.setValue(com.getItems().get(com.getSelectionModel().getSelectedIndex() - 1).getDate());
                dpTo.setValue(com.getValue().getDate());
            }
            filter(null);
        }
    }

    private static boolean LOCK = false;

    public void filter(Event e){
        if(!LOCK){
            LOCK = true;

            if(e != null && comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_APPROVAL) && ((Node)e.getSource()).getId().equals("comType")){
                com.getItems().clear();
                ObservableList<ElectionResult> a = FXCollections.observableArrayList();

                for(var s : timePeriodDao.findAll(false)){
                    // Inclusive of the last day
                    a.add(new ElectionResult(s.getTo().toLocalDate().plusDays(1)));
                }

                com.setItems(a);
                com.getSelectionModel().select(com.getItems().size() - 1);
            }

            if(e != null && comType.getSelectionModel().getSelectedItem().equals(Type.GENERIC_BALLOT) && ((Node)e.getSource()).getId().equals("comType")){
                com.getItems().clear();
                ObservableList<ElectionResult> a = FXCollections.observableArrayList();

                for(var s : timePeriodDao.findAll(true)){
                    a.add(new ElectionResult(s.getTo().toLocalDate()));
                }

                com.setItems(a);
                com.getSelectionModel().select(com.getItems().size() - 1);
            }

            if(e != null && !comStates.getValue().equals(MainController.SHOW_ALL) && ((Node)e.getSource()).getId().equals("comStates") && (comType.getValue().equals(Type.PRESIDENTIAL_GENERAL) || comType.getValue().equals(Type.GOVERNOR))){
                com.getItems().clear();
                ObservableList<ElectionResult> a = FXCollections.observableArrayList();

                if(comType.getValue().equals(Type.PRESIDENTIAL_GENERAL)){
                    a.addAll(comStates.getValue().getPresidentialDistrict().getResult());
                }else if(comType.getValue().equals(Type.GOVERNOR)){
                    a.addAll(comStates.getValue().getGubernialDistrict().getResult());
                }

                a.add(new ElectionResult(LocalDate.now().plusDays(1)));

                com.setItems(a);
                com.getSelectionModel().select(com.getItems().size() - 1);
            }

            if(e != null && !comStates.getValue().equals(MainController.SHOW_ALL) && ((Node)e.getSource()).getId().equals("comDistrict") && (comType.getValue().equals(Type.SENATE) || comType.getValue().equals(Type.HOUSE))){
                com.getItems().clear();
                ObservableList<ElectionResult> a = FXCollections.observableArrayList();

                if(comType.getValue().equals(Type.SENATE)){
                    a.addAll(comStates.getValue().getSenateDistricts().get(Integer.parseInt(comDistrict.getValue())).getResult());
                }else if(comType.getValue().equals(Type.HOUSE)){
                    a.addAll(comStates.getValue().getHouseDistricts().get(Integer.parseInt(comDistrict.getValue())).getResult());
                }

                a.add(new ElectionResult(LocalDate.now().plusDays(1)));

                com.setItems(a);
                com.getSelectionModel().select(com.getItems().size() - 1);
            }

            List<List<Object>> parameters = new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_DATE, dpFrom.getValue(), dpTo.getValue())),
                    ((comPollster.getSelectionModel().getSelectedItem().equals("Show All")) ? null : new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_POLLSTER, comPollster.getSelectionModel().getSelectedItem())))));

            if(!comType.getSelectionModel().getSelectedItem().equals(Type.SHOW_ALL))
                parameters.add(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, comType.getSelectionModel().getSelectedItem())));

            if(!comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL)){
                parameters.add(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, comStates.getSelectionModel().getSelectedItem())));
                if(e != null && !((Node)e.getSource()).getId().equals("comDistrict"))
                    setNumOfDistricts(comStates.getSelectionModel().getSelectedItem().getNumberOfActiveDistricts());
            }

            if(!comDistrict.getSelectionModel().getSelectedItem().equals("Show All"))
                parameters.add(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_DISTRICT, Integer.parseInt(comDistrict.getSelectionModel().getSelectedItem()))));

            List<Poll> result = pollDao.find(parameters);
            updateTable(result);
            setVisibility();
            if(result.size() > 0)
                displayChart();
            else
                chartManager.clear();
            LOCK = false;
        }
    }

    public void displayChart(){
        if(comDistrict.getSelectionModel().getSelectedItem() != null){
            if(comType.getSelectionModel().getSelectedItem().equals(Type.GENERIC_BALLOT) ||
                    (comType.getSelectionModel().getSelectedItem().equals(Type.HOUSE) && !comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL) && !comDistrict.getSelectionModel().getSelectedItem().equals("Show All")) ||
                    (comType.getSelectionModel().getSelectedItem().equals(Type.GOVERNOR) && !comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL)) ||
                    (comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_GENERAL) && !comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL)) ||
                    (comType.getSelectionModel().getSelectedItem().equals(Type.SENATE) && !comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL) && (!comStates.getSelectionModel().getSelectedItem().equals(MainController.GEORGIA) || !comDistrict.getSelectionModel().getSelectedItem().equals("Show All")))){
                setAverageNet(chartManager.setAverage(olPolls, LocalDate.now(), true), false);
                chartManager.createChart(olPolls, false, comType.getSelectionModel().getSelectedItem(), dpFrom.getValue(), dpTo.getValue());
            }else if(comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_APPROVAL)){
                setAverageNet(chartManager.setAverage(olPolls, LocalDate.now(), true), false);
                chartManager.createChart(olPolls, true, comType.getSelectionModel().getSelectedItem(), dpFrom.getValue(), dpTo.getValue());
            }else{
                tAverage.setText("");
                tNet.setText("");
                lcChart.getData().clear();
            }
        }
    }

    public void setAverageNet(Tuple x, boolean partySigns){
        double value = (double) Math.round((x.getX() - x.getY()) * 10) / 10;
        tAverage.setText(tvPolls.getItems().get(0).getResults().getNames().get(0) + ((partySigns) ? "(D) " : " ") + x.getX() + "%" + " - " + tvPolls.getItems().get(0).getResults().getNames().get(1) + ((partySigns) ? "(R) " : " ") + x.getY() + "%");
        tNet.setText((value > 0) ? tvPolls.getItems().get(0).getResults().getNames().get(0) + " +" + value + "%" : tvPolls.getItems().get(0).getResults().getNames().get(1) + " +" + -value + "%");
    }

    public void setVisibility(){
        if(comType.getSelectionModel().getSelectedItem().equals(Type.SHOW_ALL) || comType.getSelectionModel().getSelectedItem().equals(Type.GENERIC_BALLOT) ||
                comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_APPROVAL)){
            comStates.setDisable(true);
            comDistrict.setDisable(true);
        }else if(comType.getSelectionModel().getSelectedItem().equals(Type.SENATE) || comType.getSelectionModel().getSelectedItem().equals(Type.HOUSE) ||
                comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_GENERAL) || comType.getSelectionModel().getSelectedItem().equals(Type.GOVERNOR)){
            comStates.setDisable(false);
        }
        comDistrict.setDisable(comStates.getSelectionModel().getSelectedItem().equals(MainController.SHOW_ALL) || comStates.getSelectionModel().getSelectedItem().equals(MainController.NATIONWIDE) ||
                comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_GENERAL) || comType.getSelectionModel().getSelectedItem().equals(Type.GOVERNOR) ||
                comType.getSelectionModel().getSelectedItem().equals(Type.PRESIDENTIAL_APPROVAL));
    }

    public void bBannedOnAction(){
        ((BannedController)(Objects.requireNonNull(WindowManager.openWindow("Banned", 500, 400, true)))).start(pollDao);
    }

    public void bNewPollOnAction(){
        ((NewPollController)(Objects.requireNonNull(WindowManager.openWindow("NewPoll", 600, 500, true)))).start(pollDao);
    }

}