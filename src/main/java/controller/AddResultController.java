package controller;

import code.*;
import data.ResultNumberDao;
import data.ElectionResultDao;
import data.PersonDao;
import data.ResultPersonDao;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;
import manager.WindowManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AddResultController {

    @FXML private Button bAdd, bRemove, bDone;
    @FXML private TextField tfVotes;
    @FXML private DatePicker dpDate;
    @FXML private ComboBox<Person> comPerson;
    @FXML private TableView<Result> tvResults;
    @FXML private TableColumn<Result, String> cName, cParty, cVotes;

    private District district;

    private PersonDao personDao;
    private ElectionResultDao electionResultDao;
    private ResultNumberDao resultNumberDao;
    private ResultPersonDao resultPersonDao;

    public void start(District district, PersonDao personDao, ResultPersonDao resultPersonDao, ResultNumberDao resultNumberDao, ElectionResultDao electionResultDao){
        this.district = district;
        this.personDao = personDao;
        this.resultNumberDao = resultNumberDao;
        this.electionResultDao = electionResultDao;
        this.resultPersonDao = resultPersonDao;

        comboBoxSetUp();
        tableSetUp();
    }

    public static class Result{

        private final Person person;
        private final int votes;
        private double percent;

        public Result(Person person, int votes){
            this.person = person;
            this.votes = votes;
        }

        public double getPercent() {
            return percent;
        }

        public void setPercent(double percent) {
            this.percent = percent;
        }

        public Person getPerson() {
            return person;
        }

        public int getVotes() {
            return votes;
        }
    }

    private void tableSetUp(){
        cName.setCellValueFactory(item -> new SimpleStringProperty(item.getValue().getPerson().getName()));
        cName.setReorderable(false);
        cParty.setCellValueFactory(item -> new SimpleStringProperty(item.getValue().getPerson().getParty().getLetter()));
        cParty.setReorderable(false);
        cVotes.setCellValueFactory(item -> new SimpleStringProperty(Integer.toString(item.getValue().getVotes())));
        cVotes.setReorderable(false);
    }

    private void comboBoxSetUp(){
        ObservableList<Person> olPeople = FXCollections.observableArrayList(personDao.findAll());
        comPerson.setItems(olPeople);
    }

    public void bDoneOnAction(){
        if(tvResults.getItems().size() > 0){
            ElectionResult e = new ElectionResult(dpDate.getValue());
            e.setOwnerID(district.getID());

            List<ElectionResult.ResultPerson> people = new ArrayList<>();
            List<ElectionResult.ResultNumber> numbers = new ArrayList<>();

            for(Result r : tvResults.getItems()){
                people.add(new ElectionResult.ResultPerson(r.getPerson()));
                numbers.add(new ElectionResult.ResultNumber(r.getVotes()));
            }

            e.getNumbers().addAll(numbers);
            e.getNames().addAll(people);

            electionResultDao.create(e);

            for(ElectionResult.ResultPerson er : e.getNames()){
                er.setElectionResultID(e.getID());
                resultPersonDao.create(er);
            }

            for(ElectionResult.ResultNumber er : e.getNumbers()){
                er.setElectionResultID(e.getID());
                resultNumberDao.create(er);
            }

            district.getResult().add(e);
        }
        ((Stage)tfVotes.getScene().getWindow()).close();
    }

    public void bAddOnAction(){
        if(!comPerson.getSelectionModel().isEmpty() && !tfVotes.getText().isBlank()){
            Result r = new Result(comPerson.getValue(), Integer.parseInt(tfVotes.getText()));
            tvResults.getItems().add(r);
            tvResults.refresh();
        }
    }

    public void bRemoveOnAction(){
        if(tvResults.getSelectionModel().getSelectedItem() != null){
            tvResults.getItems().remove(tvResults.getSelectionModel().getSelectedItem());
            tvResults.refresh();
        }
    }

    public void bNewPersonOnAction(){
        ((NewPersonController)(Objects.requireNonNull(WindowManager.openWindow("NewPerson", 280, 190, true))))
                .start(personDao);
    }

}
