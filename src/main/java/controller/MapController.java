package controller;

import code.*;
import data.PollDao;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.geometry.Bounds;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;

import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.robot.Robot;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.SVGPath;
import javafx.scene.transform.Scale;
import manager.ChartManager;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.ExecutionException;

public class MapController {

    @FXML private AnchorPane apMap;
    @FXML private VBox vEastCoast, vSplits, vBox;
    @FXML private HBox hInfo;
    @FXML private Label lStateName, lPercentages, lNet;
    @FXML private Slider sSlider;
    @FXML private Button bReset, bResetAll;
    @FXML private TableView<Poll> tvPolls;
    @FXML private TableColumn<Poll, String> cDate, cPollster, cNet;

    private StateSVG currentState;
    private ChartManager chartManager;
    private PollDao pollDao;

    private Bar bar;

    private int modifiedStates = 0;

    private final List<StateSVG> states = new ArrayList<>();

    // TODO: Make the rectangles for smaller states resizable
    // FIXME: The map does not resize properly
    // TODO: Add a table showing the historic results for each state and overall
    // TODO: Somehow display tied states (color and top bar) and third party candidates
    // TODO: More elements should probably have its own class - at least StateSVG and maybe even slider and more

    public void preloadMap(PollDao pollDao){
        organizeVBoxChildren();
        setInfoBox();
        loadMap();

        createSmallerStatesRectangles("ME1", vSplits, null);
        createSmallerStatesRectangles("ME2", vSplits, null);
        createSmallerStatesRectangles("NE1", vSplits, null);
        createSmallerStatesRectangles("NE2", vSplits, null);
        createSmallerStatesRectangles("NE3", vSplits, null);

        chartManager = new ChartManager();
        this.pollDao = pollDao;

        Task<Void> task = new Task<>() {
            @Override
            public Void call() {
                for(StateSVG state : states){
                    state.assignState();
                    state.setTuple(chartManager.setAverage(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, state.getState()))))), LocalDate.now(), false));
                    state.generate();
                }
                return null;
            }
        };

        new Thread(task).start();

        try{
            task.get();
            for(StateSVG state : states){
                if((state.getId().equals("CT") || state.getId().equals("DE") || state.getId().equals("DC") ||
                    state.getId().equals("MA") || state.getId().equals("MD") || state.getId().equals("NJ") || state.getId().equals("RI"))){
                    createSmallerStatesRectangles(state.getId(), vEastCoast, state);
                }
                apMap.getChildren().add(state.getPath());
                setColor(state);
            }
            adjustElectoralBar();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }

        // FIXME: Cant change slider while nationwide is selected
        sSlider.valueProperty().addListener((observableValue, number, t1) -> {
            if(currentState != null){
                if(number.doubleValue() != 0.0 && t1.doubleValue() != 0.0){
                    double change = number.doubleValue() - t1.doubleValue();
                    if(currentState.getTuple().getX() - change > 100 || currentState.getTuple().getX() - change < 0 || currentState.getTuple().getY() + change > 100 || currentState.getTuple().getY() + change < 0)
                        return;
                    currentState.setTuple(new Tuple(currentState.getTuple().getX() - change, currentState.getTuple().getY() + change));
                    currentState.modified = true;
                    modifiedStates++;
                    bReset.setVisible(true);
                    bResetAll.setVisible(true);
                }

                displayInformation(currentState);

                sSlider.setValue(t1.doubleValue());
                adjustElectoralBar();
                setColor(currentState);
            }
        });
        displayInformation(null);
        setSlider(null);
    }

    // Set up the nodes on info box
    public void setInfoBox() {
        // Disable the reset and resetAll buttons
        bReset.setVisible(false);
        bResetAll.setVisible(false);

        // Set the columns in the latest polls table
        cDate.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getDate().format(DateTimeFormatter.ofPattern("dd/LL/yyyy"))));
        cDate.setReorderable(false);
        cPollster.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getPollster()));
        cPollster.setReorderable(false);
        cNet.setCellValueFactory(poll -> new SimpleStringProperty(((poll.getValue().getResults().getNet() > 0) ? (poll.getValue().getResults().getNames().get(0) + " +" + poll.getValue().getResults().getNet()) : poll.getValue().getResults().getNames().get(1) + " +" + Math.abs(poll.getValue().getResults().getNet()))));
        cNet.setReorderable(false);
    }

    // Organize the children of vBox so the bar is in the first position
    public void organizeVBoxChildren(){
        bar = new Bar(0, 0, "Biden", "Trump");
        ArrayList<Node> children = new ArrayList<>(vBox.getChildrenUnmodifiable());
        vBox.getChildren().clear();
        children.add(0, bar);
        vBox.getChildren().addAll(children);
    }

    public void adjustElectoralBar(){
        int D = 0; int R = 0;
        for(StateSVG state : states){
            if(state.getTuple().getX() > state.getTuple().getY())
                D += getElectoralVotes(state);
            else
                R += getElectoralVotes(state);
        }
        bar.changeNumbers(D, R);
    }

    public int getElectoralVotes(StateSVG state){
        if(state.getState().getNumberOfActiveDistricts() == 0)
            return (state.getId().equals("DC")) ? 3 : 1;
        return (state.getId().equals("ME") || state.getId().equals("NE")) ? 2 : state.getState().getNumberOfActiveDistricts() + 2;
    }

    public void bResetOnAction(){
        currentState.setTuple(chartManager.setAverage(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, currentState.getState()))))), LocalDate.now(), false));
        displayInformation(currentState);
        adjustElectoralBar();
        setColor(currentState);
        currentState.modified = false;
        modifiedStates--;
        bReset.setVisible(false);
        bResetAll.setVisible(modifiedStates > 0);
        sSlider.setValue(0.0);
        sSlider.setValue(currentState.getTuple().getX());
    }

    public void bResetAllOnAction(){
        Task<Void> task = new Task<>() {
            @Override
            public Void call() {
                for(StateSVG state : states){
                    state.setTuple(chartManager.setAverage(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, state.getState()))))), LocalDate.now(), false));
                    state.modified = false;
                    modifiedStates = 0;
                    bReset.setVisible(false);
                    bResetAll.setVisible(false);
                }
                return null;
            }
        };

        new Thread(task).start();

        try{
            task.get();

            if(currentState != null){
                displayInformation(currentState);
                sSlider.setValue(0.0);
                sSlider.setValue(currentState.getTuple().getX());
            }
            for(StateSVG state : states){
                setColor(state);
            }
            adjustElectoralBar();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
    }

    public void createSmallerStatesRectangles(String id, VBox box, StateSVG state){
        if(state == null){
            state = new StateSVG();
            state.setId(id);
            states.add(state);
        }
        state.setRec(new Rectangle(45, 20));
        state.getRec().setStyle("-fx-stroke: black; -fx-stroke-width: 1;");
        state.setOnMouse(state.getRec());

        box.getChildren().addAll(new Label(state.getId()), state.getRec());
    }

    public void setColor(StateSVG state){
        if(state.getPath() != null)
            state.getPath().setFill(generateColor(state));
        if(state.getRec() != null)
            state.getRec().setFill(generateColor(state));
    }

    public Color generateColor(StateSVG state){
        Color color;
        double value = state.getTuple().getNet();
        int gradient = 240 - (int) (Math.abs(value) * 16);
        if(Math.abs(value) > 15)
            color = (value <= 0) ? Color.rgb(255, 0, 0) : Color.rgb(0, 0, 255);
        else
            color = (value <= 0) ? Color.rgb(255, gradient, gradient) : Color.rgb(gradient, gradient, 255);
        return color;
    }

    public void loadMap(){
        try(BufferedReader br = new BufferedReader(new InputStreamReader(Objects.requireNonNull(MapController.class.getResourceAsStream("/gfx/us.svg")), StandardCharsets.UTF_8))) {
            String line = br.readLine();
            StateSVG state = null;
            while (line != null) {
                if(line.startsWith("  <path")){
                    state = new StateSVG();
                    states.add(state);
                }else {
                    var substring = line.substring(line.indexOf("=") + 2, line.length() - 1);
                    if(line.startsWith("     id") && state != null){
                        state.setId(substring);
                    }else if(line.startsWith("     d") && state != null){
                        state.setContent(substring);
                    }
                }
                line = br.readLine();
            }
        }catch(IOException e ){
            e.printStackTrace();
        }
    }

    // If state is null then display nationwide information
    public void displayInformation(StateSVG state) {
        lStateName.setText((state == null) ? "Nationwide" : (state.getState().getFullName() + " (" + getElectoralVotes(state) + ")"));
        var tuple = (state == null) ? chartManager.setAverage(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, MainController.NATIONWIDE))))), LocalDate.now(), false) : state.getTuple();
        lPercentages.setText("Biden " + tuple.getX() + "%" + " - " + tuple.getY() + "%" + " Trump");
        lNet.setText(((tuple.getX() > tuple.getY()) ? "Biden +" + ((double) Math.round((Math.abs(tuple.getNet())) * 10) / 10) + "%" : "Trump +" + ((double) Math.round((Math.abs(tuple.getNet())) * 10) / 10) + "%"));

        // Display the latest polls
        // TODO: The StateSVG object should probably know the latest polls

        ObservableList<Poll> polls = FXCollections.observableArrayList(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, (state == null) ? MainController.NATIONWIDE : state.getState()))))));
        tvPolls.setItems(polls);
    }

    public void setSlider(StateSVG state) {
        var tuple = (state == null) ? chartManager.setAverage(pollDao.find(new ArrayList<>(Arrays.asList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_TYPE, Type.PRESIDENTIAL_GENERAL)), new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_STATE, MainController.NATIONWIDE))))), LocalDate.now(), false) : state.getTuple();
        // Set the slider max to the combined value of both percentages
        sSlider.setMax(tuple.getX() + tuple.getY());
        // If there are no polls then set it to 100
        if(sSlider.getMax() == 0)
            sSlider.setMax(100);
        sSlider.setValue(0.0);
        sSlider.setValue(tuple.getX());
    }

    public class StateSVG {
        private String id, content;
        private SVGPath path;
        private State state;
        private Rectangle rec;
        private Tuple tuple;

        private boolean keepStroke = false;
        private boolean modified = false;

        public StateSVG(){ }

        public void assignState(){
            for(State s : MainController.states){
                if(s.getTwoLetters().equals(id)){
                    state = s;
                    break;
                }
            }
        }

        public void resizeMap(Scene scene){
            Scale scale = new Scale(scene.getWidth() / 1000, scene.getHeight() / 800);
            scale.setPivotX(100);
            scale.setPivotY(100);
            path.getTransforms().setAll(scale);
        }

        public void generate(){
            path = new SVGPath();
            path.setId(id);
            if(content != null){
                path.setContent(content);
                setOnMouse(path);
            }else
                setOnMouse(rec);
        }

        public void setOnMouse(Node n){
            n.setOnMouseClicked(this::setOnMouseClicked);
            n.setOnMouseEntered(this::setOnMouse);
            n.setOnMouseExited(this::setOnMouseExited);
        }

        public void setOnMouseExited(MouseEvent e){
            if(!keepStroke){
                path.setStroke(null);
                currentState = null;
                // Display the nationwide info
                displayInformation(null);
                setSlider(null);
            }
        }

        // Moves the mouse cursor, on mouse clicked, to the state details.
        public void setOnMouseClicked(MouseEvent e){
            keepStroke = true;
            Bounds boundsInScene = lStateName.localToScreen(lStateName.getBoundsInLocal());
            new Robot().mouseMove(boundsInScene.getCenterX(), boundsInScene.getCenterY());
        }

        public void setOnMouse(MouseEvent e){
            if(currentState != null)
                currentState.path.setStroke(null);
            keepStroke = false;

            path.setStrokeWidth(2.5);
            path.setStroke(Color.YELLOW);

            if(!hInfo.isVisible())
                hInfo.setVisible(true);

            currentState = this;

            bReset.setVisible(modified);
            bResetAll.setVisible(modifiedStates > 0);

            displayInformation(this);
            setSlider(this);
        }

        public String getId() { return id; }

        public String getContent() { return content; }

        public SVGPath getPath() { return path; }

        public void setId(String id) { this.id = id; }

        public void setContent(String content) { this.content = content; }

        public State getState() { return state; }

        public Rectangle getRec() { return rec; }

        public void setRec(Rectangle rec) { this.rec = rec; }

        public Tuple getTuple() { return tuple; }

        public void setTuple(Tuple tuple) { this.tuple = tuple; }
    }

    public void resize(Scene scene){
        for(StateSVG state : states){
            state.resizeMap(scene);
        }
    }

}