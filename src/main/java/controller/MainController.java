package controller;

import code.*;
import data.*;
import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import org.apache.derby.jdbc.EmbeddedDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class MainController {

    @FXML private TabPane tpTabPane;
    @FXML private AnchorPane apMap, apPolls, apSettings;

    private MapController mapController;

    private static int tabIndex = 0;
    public static List<State> states = new ArrayList<>();
    // TODO: This cant stay like this

    public static State NATIONWIDE;
    public static State SHOW_ALL;
    public static State GEORGIA;

    // TODO: Add House and Senate (Gubernial) maps

   /* public static List<String> readFile(String file){
        List<String> text = new ArrayList<>();
        try(BufferedReader br = new BufferedReader(new FileReader(file, StandardCharsets.UTF_8))) {
            String line = br.readLine();
            while (line != null) {
                text.add(line);
                line = br.readLine();
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return text;
    }

    public static Poll createPoll(String[] parts){
        String[] date = parts[0].split("/");
        Poll p = new Poll(LocalDateTime.of(Integer.parseInt(date[2]), Integer.parseInt(date[1]), Integer.parseInt(date[0]), Integer.parseInt(date[3]), Integer.parseInt(date[4])),
                parts[1], parts[2], Type.getType(parts[3]), parts[4], Integer.parseInt(parts[5]), parts[6], parts[7], false);
        int current = 8;
        while(current < parts.length){
            p.getResults().add(parts[current], Integer.parseInt(parts[current + 1]));
            current += 2;
        }
        return p;
    }

    public static void load(String file, PollDao p){
        List<String> text = readFile(file);
        for(String line : text){
            p.create(createPoll(line.split(";")));
        }
    }*/

    public void start(HostServices hostServices){
        var dataSource = createDataSource();
        PollDao pollDao = new PollDao(dataSource);

        //load("polls.txt", pollDao);

        TimePeriodDao timePeriodDao = new TimePeriodDao(dataSource);

        PersonDao personDao = new PersonDao(dataSource);
        ResultNumberDao resultNumberDao = new ResultNumberDao(dataSource);
        ResultPersonDao resultPersonDao = new ResultPersonDao(dataSource, personDao);
        ElectionResultDao electionResultDao = new ElectionResultDao(dataSource, resultPersonDao, resultNumberDao);

        DistrictDao districtDao = new DistrictDao(dataSource, electionResultDao);
        StateDao stateDao = new StateDao(dataSource, districtDao);

        tpTabPane.getSelectionModel().select(tabIndex);

        // Creating the states
        /*stateDao.create(new State("Alabama", "Ala.", "AL"));
        stateDao.create(new State("Alaska", "Alaska", "AK"));
        stateDao.create(new State("Arizona", "Ariz.", "AZ"));
        stateDao.create(new State("Arkansas", "Ark.", "AR"));
        stateDao.create(new State("California", "Calif.", "CA"));
        stateDao.create(new State("Colorado", "Colo.", "CO"));
        stateDao.create(new State("Connecticut", "Conn.", "CT"));
        stateDao.create(new State("Delaware", "Del.", "DE"));
        stateDao.create(new State("Florida", "Fla.", "FL"));
        stateDao.create(new State("D.C.", "D.C.", "DC"));
        stateDao.create(new State("Georgia", "Ga.", "GA"));
        stateDao.create(new State("Hawaii", "Hawaii", "HI"));
        stateDao.create(new State("Iowa", "Iowa", "IA"));
        stateDao.create(new State("Idaho", "Idaho", "ID"));
        stateDao.create(new State("Illinois", "Ill.", "IL"));
        stateDao.create(new State("Indiana", "Ind.", "IN"));
        stateDao.create(new State("Kansas", "Kan.", "KS"));
        stateDao.create(new State("Kentucky", "Ky.", "KY"));
        stateDao.create(new State("Louisiana", "La.", "LA"));
        stateDao.create(new State("Massachusetts", "Mass.", "MA"));
        stateDao.create(new State("Maryland", "Md.", "MD"));
        stateDao.create(new State("Maine", "Maine", "ME"));
        stateDao.create(new State("ME-1", "ME-1", "ME1"));
        stateDao.create(new State("ME-2", "ME-2", "ME2"));
        stateDao.create(new State("Michigan", "Mich.", "MI"));
        stateDao.create(new State("Minnesota", "Minn.", "MN"));
        stateDao.create(new State("Missouri", "Mo.", "MO"));
        stateDao.create(new State("Mississippi", "Miss.", "MS"));
        stateDao.create(new State("Montana", "Mont.", "MT"));
        stateDao.create(new State("North Carolina", "N.C.", "NC"));
        stateDao.create(new State("North Dakota", "N.D.", "ND"));
        stateDao.create(new State("Nebraska", "Neb.", "NE"));
        stateDao.create(new State("NE-1", "NE-1", "NE1"));
        stateDao.create(new State("NE-2", "NE-2", "NE2"));
        stateDao.create(new State("NE-3", "NE-3", "NE3"));
        stateDao.create(new State("New Hampshire", "N.H.", "NH"));
        stateDao.create(new State("New Jersey", "N.J.", "NJ"));
        stateDao.create(new State("New Mexico", "N.M.", "NM"));
        stateDao.create(new State("Nevada", "Nev.", "NV"));
        stateDao.create(new State("New York", "N.Y.", "NY"));
        stateDao.create(new State("Ohio", "Ohio", "OH"));
        stateDao.create(new State("Oklahoma", "Okla.", "OK"));
        stateDao.create(new State("Oregon", "Ore.", "OR"));
        stateDao.create(new State("Pennsylvania", "Pa.", "PA"));
        stateDao.create(new State("Rhode Island", "R.I.", "RI"));
        stateDao.create(new State("South Carolina", "S.C.", "SC"));
        stateDao.create(new State("South Dakota", "S.D.", "SD"));
        stateDao.create(new State("Tennessee", "Tenn.", "TN"));
        stateDao.create(new State("Texas", "Texas", "TX"));
        stateDao.create(new State("Utah", "Utah", "UT"));
        stateDao.create(new State("Virginia", "Va.", "VA"));
        stateDao.create(new State("Vermont", "Vt.", "VT"));
        stateDao.create(new State("Washington", "Wash.", "WA"));
        stateDao.create(new State("Wisconsin", "Wis.", "WI"));
        stateDao.create(new State("West Virginia", "W.Va.", "WV"));
        stateDao.create(new State("Wyoming", "Wyo.", "WY"));*/

        states = stateDao.findAll();

        //personDao.dropTable();
        /*resultNumberDao.dropTable();
        resultPersonDao.dropTable();
        electionResultDao.dropTable();*/

        // Creating the gubernial and presidential districts
        /*for(State s : states){
            District p = new District(1);
            District g = new District(10);

            p.setStateID(s.getID());
            g.setStateID(s.getID());

            districtDao.create(p);
            districtDao.create(g);

            s.setPresidentialDistrict(p);
            s.setGubernialDistrict(g);
        }*/

        // Creating the senate seats
        /*for(State state : states){
            if(state.getNumberOfActiveDistricts() != 0){
                for(int x = 0; x < 2; x++){
                    District d = new District(100 + state.getSenateDistricts().size() + 1);
                    d.setStateID(state.getID());
                    districtDao.create(d);
                    state.getSenateDistricts().add(d);
                }
            }
        }*/

        SHOW_ALL = new State("Show All", "Show All", "Show All");
        NATIONWIDE = new State("Nationwide", "Nationwide", "Nationwide");

        GEORGIA = states.get(12);

        ((PollsController)(Objects.requireNonNull(load("Polls", apPolls)))).start(pollDao, hostServices, timePeriodDao);
        mapController = (MapController)(Objects.requireNonNull(load("Map", apMap)));
        mapController.preloadMap(pollDao);
        ((SettingsController)(Objects.requireNonNull(load("Settings", apSettings)))).start(timePeriodDao, districtDao, personDao, electionResultDao, resultNumberDao, resultPersonDao);

        tpTabPane.setOnMousePressed(e -> {
            if (tabIndex != tpTabPane.getSelectionModel().getSelectedIndex()) {
                tabIndex = tpTabPane.getSelectionModel().getSelectedIndex();
                tpTabPane.getSelectionModel().select(tabIndex);
            }
        });

    }

    private Object load(String name, AnchorPane ap){
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/" + name + ".fxml"));

            Node n = fxmlLoader.load();
            setAnchors(n);
            ap.getChildren().add(n);

            return fxmlLoader.getController();
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

    public void setAnchors(Node n){
        AnchorPane.setTopAnchor(n, 0.0);
        AnchorPane.setRightAnchor(n, 0.0);
        AnchorPane.setLeftAnchor(n, 0.0);
        AnchorPane.setBottomAnchor(n, 0.0);
    }

    private static DataSource createDataSource() {
        String dbPath = System.getProperty("user.home") + "/RealElectionPolls";
        EmbeddedDataSource dataSource = new EmbeddedDataSource();
        dataSource.setDatabaseName(dbPath);
        dataSource.setCreateDatabase("create");
        return dataSource;
    }

    public MapController getMapController() {
        return mapController;
    }
}