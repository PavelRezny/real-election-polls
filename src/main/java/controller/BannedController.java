package controller;

import code.Poll;
import data.PollDao;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;

import java.time.format.DateTimeFormatter;
import java.util.*;

public class BannedController {

    @FXML private TableView<String> tvPollsters;
    @FXML private TableView<Poll> tvPolls;
    @FXML private TableColumn<String, String> cPollsterName;
    @FXML private TableColumn<Poll, String> cPollDate, cPollName, cPollNet, cPollType, cPollState;

    private ObservableList<Poll> olPolls;
    private ObservableList<String> olPollsters;

    private ContextMenu menuPolls, menuPollsters;
    private MenuItem removePolls, removePollsters;

    private PollDao pollDao;

    public void start(PollDao pollDao) {
        this.pollDao = pollDao;

        setTable();
        setContextMenu();
        mouseHandler();
    }

    public void mouseHandler(){
        tvPolls.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            if(e.getButton() == MouseButton.SECONDARY){
                menuPolls.show(tvPolls, e.getScreenX(), e.getScreenY());
            }else{
                menuPolls.hide();
            }
        });

        tvPollsters.addEventHandler(MouseEvent.MOUSE_CLICKED, e -> {
            if(e.getButton() == MouseButton.SECONDARY){
                menuPollsters.show(tvPollsters, e.getScreenX(), e.getScreenY());
            }else{
                menuPollsters.hide();
            }
        });

        removePolls.setOnAction(e -> {
            if(tvPolls.getSelectionModel().getSelectedItem() != null){
                Poll p = tvPolls.getSelectionModel().getSelectedItem();
                p.setBanned(false);
                Task<Void> task = new Task<>() {
                    @Override
                    public Void call() {
                        pollDao.update(p);
                        return null;
                    }
                };

                new Thread(task).start();

                olPolls.remove(p);
                tvPolls.refresh();
            }
        });

        removePollsters.setOnAction(e -> {
            if(tvPollsters.getSelectionModel().getSelectedItem() != null){
                List<Poll> toUnban = new ArrayList<>();
                String pollster = tvPollsters.getSelectionModel().getSelectedItem();
                for(Poll p : pollDao.find(new ArrayList<>(Collections.singletonList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_BANNED_POLLS, true)))))){
                    if(p.getPollster().equals(pollster)){
                        p.setBanned(false);
                        toUnban.add(p);
                        olPolls.remove(p);
                    }
                }

                Task<Void> task = new Task<>() {
                    @Override
                    public Void call() {
                        for(Poll p : toUnban){
                            pollDao.update(p);
                        }
                        return null;
                    }
                };
                new Thread(task).start();

                olPollsters.remove(pollster);
                tvPollsters.refresh();
                tvPolls.refresh();
            }
        });
    }

    public void setTable(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/LL/yyyy");
        cPollDate.setCellValueFactory(poll -> new SimpleStringProperty(formatter.format(poll.getValue().getDate())));
        cPollDate.setReorderable(false);
        cPollName.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getPollster()));
        cPollName.setReorderable(false);
        cPollNet.setCellValueFactory(poll -> new SimpleStringProperty((poll.getValue().getResults().getNet() > 0) ? "+" + poll.getValue().getResults().getNet() : Integer.toString(poll.getValue().getResults().getNet())));
        cPollNet.setReorderable(false);
        cPollType.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getPollType().getFormattedText()));
        cPollType.setReorderable(false);
        cPollState.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue().getState()));
        cPollState.setReorderable(false);

        List<Poll> allPolls = pollDao.find(new ArrayList<>(Collections.singletonList(new ArrayList<>(Arrays.asList(PollDao.SQLString.SQL_BANNED_POLLS, true)))));
        List<Poll> bannedPolls = new ArrayList<>();
        Set<String> bannedPollsters = new HashSet<>();

        for(Poll p : allPolls){
            if(p.isBanned()) {
                bannedPolls.add(p);
                bannedPollsters.add(p.getPollster());
            }
        }

        olPolls = FXCollections.observableArrayList(bannedPolls);
        tvPolls.setItems(olPolls);
        tvPolls.refresh();

        cPollsterName.setCellValueFactory(poll -> new SimpleStringProperty(poll.getValue()));
        cPollsterName.setReorderable(false);

        olPollsters = FXCollections.observableArrayList(bannedPollsters);
        tvPollsters.setItems(olPollsters);
        tvPollsters.refresh();
    }

    public void setContextMenu(){
        menuPolls = new ContextMenu();
        removePolls = new MenuItem("Remove");
        menuPolls.getItems().addAll(removePolls);

        menuPollsters = new ContextMenu();
        removePollsters = new MenuItem("Remove");
        menuPollsters.getItems().addAll(removePollsters);
    }
}