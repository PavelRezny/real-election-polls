package controller;

import code.Poll;
import code.Type;
import data.PollDao;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

public class NewPollController {

    @FXML private Button bCreate;
    @FXML private DatePicker dpDate;
    @FXML private ComboBox<Type> comType;
    @FXML private TextField tfSampleSize, tfSampleType, tfPollster, tfState, tfURL, tfDateConducted, tfAName, tfANumber, tfBName, tfBNumber;

    private PollDao pollDao;

    public void start(PollDao pollDao){
        this.pollDao = pollDao;

        setComboBox();
    }

    private void setComboBox(){
        comType.setItems(FXCollections.observableArrayList(Type.values()));
    }

    public void bCreateOnAction(){
        Poll p = new Poll(
                dpDate.getValue().atStartOfDay(),
                tfDateConducted.getText(),
                tfPollster.getText(),
                comType.getValue(),
                tfSampleType.getText(),
                Integer.parseInt(tfSampleSize.getText()),
                tfState.getText(),
                tfURL.getText(),
                true
        );

        p.getResults().add(tfAName.getText(), Integer.parseInt(tfANumber.getText()));
        p.getResults().add(tfBName.getText(), Integer.parseInt(tfBNumber.getText()));

        pollDao.create(p);
        ((Stage)tfAName.getScene().getWindow()).close();
    }

}
