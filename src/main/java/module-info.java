module RealElectionPolls {
    requires javafx.fxml;
    requires javafx.controls;
    requires commons.math3;
    requires org.jsoup;
    requires java.desktop;
    requires java.sql;
    requires derby;
    requires java.naming;
    opens code;
    opens controller;
    opens manager;
}