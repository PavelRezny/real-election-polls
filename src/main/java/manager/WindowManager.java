package manager;

import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.Objects;

public class WindowManager {

    public static Object openWindow(String name, int width, int height, boolean modal){
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(WindowManager.class.getResource("/fxml/" + name +".fxml"));
            Parent root = fxmlLoader.load();

            Stage stage = new Stage();
            if(modal)
                stage.initModality(Modality.APPLICATION_MODAL);
            stage.setTitle("Real Election Polls");
            stage.getIcons().add(new Image(Objects.requireNonNull(WindowManager.class.getResourceAsStream("/gfx/icon.png"))));
            stage.setScene(new Scene(root, width , height));

            stage.show();

            return fxmlLoader.getController();
        }catch(IOException e){
            e.printStackTrace();
        }
        return null;
    }

}
