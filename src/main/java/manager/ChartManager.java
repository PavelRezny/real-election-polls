package manager;

import code.Filters;
import code.Poll;
import code.Tuple;
import code.Type;
import javafx.geometry.Insets;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.text.TextAlignment;
import org.apache.commons.math3.stat.descriptive.moment.Mean;

import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class ChartManager {

    private LineChart<String, Number> lcChart;
    private CategoryAxis caAxis;
    private NumberAxis naAxis;

    private final Filters filters;

    public ChartManager(){
        filters = new Filters();
    }

    public ChartManager(LineChart<String, Number> lcChart, CategoryAxis caAxis, NumberAxis naAxis){
        this.lcChart = lcChart;
        this.caAxis = caAxis;
        this.naAxis = naAxis;

        filters = new Filters();

    }

    public void clear(){
        lcChart.getData().clear();
    }

    // TODO: Add an option for more than two lines

    public void createChart(List<Poll> polls, boolean approval, Type selectedType, LocalDate from, LocalDate to){
        lcChart.getData().clear();

        XYChart.Series<String, Number> dataSeries1 = new XYChart.Series<>();
        dataSeries1.setName(polls.get(0).getResults().getNames().get(0));
        XYChart.Series<String, Number> dataSeries2 = new XYChart.Series<>();
        dataSeries2.setName(polls.get(0).getResults().getNames().get(1));

        // TODO: Optimize the data loading

        while(!from.equals(to.plus(1, ChronoUnit.DAYS))){
            Tuple a = setAverage(polls, from, false);
            dataSeries1.getData().add(new XYChart.Data<>(from.toString(), a.getX()));
            dataSeries2.getData().add(new XYChart.Data<>(from.toString(), a.getY()));
            from = from.plus(1, ChronoUnit.DAYS);
        }

        // TODO: Make the tooltip work better

        int min = 100, max = 0;
        int x = 0;
        for(XYChart.Data<String, Number> a : dataSeries1.getData()){
            double value = a.getYValue().doubleValue() - dataSeries2.getData().get(x).getYValue().doubleValue();
            value = (double) Math.round((value) * 10) / 10;
            if((value >= 0)){
                if(selectedType.equals(Type.PRESIDENTIAL_APPROVAL)){
                    a.setNode(new HoveredThresholdNode(("App." + "\n" + "+" + value)));
                }else{
                    a.setNode(new HoveredThresholdNode(("(D)" + "\n" + "+" + value)));
                }
                a.getNode().setOpacity(0);
            }
            if(a.getYValue().intValue() < min) {
                min = a.getYValue().intValue();
            }else if(a.getYValue().intValue() > max){
                max = a.getYValue().intValue();
            }
            x++;
        }
        x = 0;
        for(XYChart.Data<String, Number> a : dataSeries2.getData()){
            double value = a.getYValue().doubleValue() - dataSeries1.getData().get(x).getYValue().doubleValue();
            value = (double) Math.round((value) * 10) / 10;
            if((value >= 0)){
                if(selectedType.equals(Type.PRESIDENTIAL_APPROVAL)){
                    a.setNode(new HoveredThresholdNode(("Dis." + "\n" + "+" + value)));
                }else{
                    a.setNode(new HoveredThresholdNode(("(R)" + "\n" + "+" + value)));
                }
                a.getNode().setOpacity(0);
            }
            if(a.getYValue().intValue() < min) {
                min = a.getYValue().intValue();
            }else if(a.getYValue().intValue() > max){
                max = a.getYValue().intValue();
            }
            x++;
        }

        naAxis.setLowerBound(min - 1);
        naAxis.setUpperBound(max + 1);

        if(approval){
            lcChart.getStylesheets().add(String.valueOf(getClass().getResource("/css/chartApprovalColors.css")));
        }else{
            lcChart.getStylesheets().add(String.valueOf(getClass().getResource("/css/chartPartyColors.css")));
        }

        lcChart.getData().addAll(dataSeries1, dataSeries2);
    }

    private static class HoveredThresholdNode extends Pane {

        public HoveredThresholdNode(String object) {
            setPrefSize(10, 10);
            final Label label = createDataThresholdLabel(object);
            label.setTextAlignment(TextAlignment.CENTER);
            setOnMouseEntered(mouseEvent -> {
                setOpacity(100);
                getChildren().setAll(label);
                label.setPadding(new Insets(30, 0, 0, 0));
                toFront();
            });
            setOnMouseExited(mouseEvent -> {
                setOpacity(0);
                getChildren().clear();
            });
        }

        private Label createDataThresholdLabel(Object object) {
            final Label label = new Label(object + "");
            label.getStyleClass().addAll("default-color0", "chart-line-symbol", "chart-series-line");
            label.setStyle("-fx-font-size: 10; -fx-font-weight: bold; -fx-font-family: verdana; -fx-background-color: linear-gradient(to bottom, rgba(248,80,50,0) 0%, rgba(255,255,255,0) 50%, rgba(255,255,255,1) 51%, rgba(255,255,255,1) 61%, rgba(254,250,250,0) 62%, rgba(231,56,39,0) 100%);");
            label.setTextFill(Color.BLACK);
            label.setMinSize(Label.USE_PREF_SIZE, Label.USE_PREF_SIZE);
            return label;
        }
    }

    public Tuple setAverage(List<Poll> _polls, LocalDate date, boolean highlight){
        List<Poll> polls = new ArrayList<>(_polls);
        polls.removeIf(p -> p.isBanned() || p.getSampleType().equals("A"));

        List<Poll> afterBanned = new ArrayList<>(polls);
        filters.removeOutOfTimeFramePolls(polls, date);

        List<Poll> result = filters.removeMultiplePollsBySamePollster(polls);
        if(polls.size() == 0)
            polls.addAll(afterBanned);

        if(result.size() < 3){
            for(Poll p : polls){
                boolean add = true;
                for(Poll _p : result){
                    if (p.getDate().equals(_p.getDate()) && p.getPollster().equals(_p.getPollster())) {
                        add = false;
                        break;
                    }
                }
                if(add)
                    result.add(p);
                if(result.size() == 3)
                    break;
            }
        }

        if(result.size() > 10){
            ListIterator<Poll> iterator = result.listIterator();
            while (iterator.hasNext()){
                Poll p = iterator.next();
                if(result.indexOf(p) >= 10)
                    iterator.remove();
            }
        }

        if(highlight){
            for(Poll p : result){
                p.setHighlight(true);
            }
        }

        if(result.size() > 0){
            return new Tuple((double) Math.round((calculateWeightedMean(result, 0)) * 10) / 10, (double) Math.round((calculateWeightedMean(result, 1)) * 10) / 10);
        }else{
            return new Tuple(0,0);
        }
    }

    public double calculateWeightedMean(List<Poll> polls, int y){
        Mean m = new Mean();
        LocalDate current = polls.get(0).getDate().toLocalDate();
        double[] weights = new double[polls.size()];
        double[] values = new double[polls.size()];
        for(int x = 0; x < polls.size(); x++){
            LocalDate d1 = LocalDate.parse(current.toString(), DateTimeFormatter.ISO_LOCAL_DATE);
            LocalDate now = polls.get(x).getDate().toLocalDate();
            LocalDate d2 = LocalDate.parse(now.toString(), DateTimeFormatter.ISO_LOCAL_DATE);
            Duration diff = Duration.between(d1.atStartOfDay(), d2.atStartOfDay());
            long diffDays = diff.toDays();
            weights[x] = 100.0 + ((diffDays * 10 < 0) ? 0 : (diffDays * 10));
            values[x] = (double)(polls.get(x).getResults().getNumbers().get(y));
        }
        return m.evaluate(values, weights);
    }
}
