package code;

import controller.MainController;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import java.io.IOException;
import java.util.Objects;

public class Main extends Application {

    boolean width = false, height = false;

    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("/fxml/Main.fxml"));
        Parent root = fxmlLoader.load();

        MainController controller = fxmlLoader.getController();
        controller.start(getHostServices());

        Scene scene = new Scene(root, 1000, 800);

        scene.widthProperty().addListener((observableValue, oldSceneWidth, newSceneWidth) -> {
            width = true;
            if(newSceneWidth.intValue() > 800 && width && height){
                controller.getMapController().resize(scene);
                width = false;
                height = false;
            }
        });

        scene.heightProperty().addListener((observableValue, oldSceneHeight, newSceneHeight) -> {
            height = true;
            if(newSceneHeight.intValue() > 500 && width && height){
                controller.getMapController().resize(scene);
                width = false;
                height = false;
            }
        });

        primaryStage.setTitle("Real Election Polls");
        primaryStage.setScene(scene);
        primaryStage.getIcons().add(new Image(Objects.requireNonNull(getClass().getResourceAsStream("/gfx/icon.png"))));
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
