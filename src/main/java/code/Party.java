package code;

public enum Party {

    D("D", "DEM", "Democratic Party"),
    R("R","REP", "Republican Party"),
    I("I", "IND", "Independent"),
    L("L", "LIB", "Libertarian"),
    G("G", "GRE", "Green Party"),
    O("O", "OTH", "Others");

    private final String letter, shortName, name;

    Party(String letter, String shortName, String name){
        this.letter = letter;
        this.shortName = shortName;
        this.name = name;
    }

    public String getLetter() {
        return letter;
    }

    public String getShortName() {
        return shortName;
    }

    public String getName() {
        return name;
    }
}
