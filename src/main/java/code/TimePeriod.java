package code;

import java.time.LocalDateTime;

public class TimePeriod {

    private Long ID;
    private final LocalDateTime from, to;
    private final String name;

    public TimePeriod(LocalDateTime from, LocalDateTime to, String name){
        this.from = from;
        this.to = to;
        this.name = name;
    }

    public LocalDateTime getFrom() {
        return from;
    }

    public LocalDateTime getTo() {
        return to;
    }

    public String getName() {
        return name;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
}
