package code;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;

public class Filters {

    public List<Poll> getSameDayPolls(List<Poll> polls, LocalDate date, String pollster){
        List<Poll> result = new ArrayList<>();
        for(Poll p : polls){
            if(p.getPollster().equals(pollster) && p.getDate().equals(date)){
                result.add(p);
            }
        }
        return result;
    }

    public Poll getLowestUndecided(List<Poll> polls){
        Poll lowestUndecided = null;
        for(Poll p : polls){
            if(lowestUndecided == null){
                lowestUndecided = p;
            }else if(lowestUndecided.getResults().getUndecided() > p.getResults().getUndecided()){
                lowestUndecided = p;
            }
        }
        return lowestUndecided;
    }

    public List<Poll> removeMultiplePollsBySamePollster(List<Poll> polls){
        List<Poll> tmp = new ArrayList<>();
        for(Poll p : polls){
            List<Poll> day = getSameDayPolls(polls, p.getDate().toLocalDate(), p.getPollster());
            if(day.size() > 1){
                boolean LV = false, RV = false;
                for(Poll poll : day){
                    if(poll.getSampleType().equals("LV")){
                        LV = true;
                    }else if(poll.getSampleType().equals("RV")){
                        RV = true;
                    }
                }
                if(LV && RV){
                    day.removeIf(pp -> pp.getSampleType().equals("RV"));
                }
                tmp.add(getLowestUndecided(day));
            }else{
                tmp.addAll(day);
            }
        }

        List<Poll> result = new ArrayList<>();
        for(Poll p : tmp){
            boolean add = true;
            for(Poll in : result){
                if (p.getPollster().equals(in.getPollster())) {
                    add = false;
                    break;
                }
            }
            if(add)
                result.add(p);
        }

        return result;
    }

    public void removeOutOfTimeFramePolls(List<Poll> polls, LocalDate date){
        ListIterator<Poll> iterator = polls.listIterator();
        while(iterator.hasNext()){

            LocalDate from = LocalDate.from(date.atStartOfDay(ZoneId.systemDefault())).minus(7, ChronoUnit.DAYS);
            LocalDate _date = LocalDate.from(date.atStartOfDay(ZoneId.systemDefault()).plusDays(1).minusMinutes(1));

            Poll p = iterator.next();

            if((p.getDate().toLocalDate().isBefore(from) || p.getDate().toLocalDate().isAfter(_date)))
                iterator.remove();
        }
    }

}
