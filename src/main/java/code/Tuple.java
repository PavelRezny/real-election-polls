package code;

public class Tuple {

    private final double x, y;

    public Tuple(double x, double y){
        // Rounds the numbers to one decimal place
        int scale = (int) Math.pow(10, 1);
        this.x = (double) Math.round(x * scale) / scale;
        this.y = (double) Math.round(y * scale) / scale;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public double getNet(){
        return x - y;
    }

}
