package code;

public enum Type {

    // TODO: Add primaries

    SHOW_ALL("show-all", "Show All"),
    PRESIDENTIAL_APPROVAL("approval", "Presidential Approval"),
    PRESIDENTIAL_GENERAL("president-general", "Presidential General"),
    SENATE("senate", "Senate"),
    GOVERNOR("governor", "Governor"),
    HOUSE("house", "House"),
    GENERIC_BALLOT("generic-ballot","Generic Ballot"),
    UNKNOWN("unknown", "Unknown");

    private final String originalText, formattedText;

    Type(String originalText, String formattedText){
        this.originalText = originalText;
        this.formattedText = formattedText;
    }

    public String getOriginalText() {
        return originalText;
    }

    public String getFormattedText() {
        return formattedText;
    }

    public String toString(){
        return formattedText;
    }

    public static Type getType(String value){
        for(Type t : Type.values()){
            if(t.getOriginalText().equals(value)){
                return t;
            }
        }
        return UNKNOWN;
    }

}
