package code;

import controller.MainController;

import java.time.LocalDateTime;
import java.util.*;

public class Poll{

    // TODO: Pollster rating

    private Long ID;
    private String dateConducted;
    private LocalDateTime date;
    private Type pollType;
    private String pollster, sampleType, state, url;
    private Integer district;
    private int sampleSize;
    private final Result results = new Result();
    private boolean custom = false;
    private boolean highlight, _new = false, banned = false;

    public Poll(){}

    public Poll(LocalDateTime date, String dateConducted, String pollster, Type pollType, String sampleType, int sampleSize, String state, String url, boolean custom){
        this.date = date;
        this.dateConducted = dateConducted;
        this.pollster = pollster;
        this.pollType = pollType;
        this.sampleType = sampleType;
        this.sampleSize = sampleSize;
        this.state = state;
        this.url = url;
        this.custom = custom;
    }

    public static class Result{

        private final List<String> names = new ArrayList<>();
        private final List<Integer> numbers = new ArrayList<>();

        public Result(){ }

        public void add(String name, int number){
            names.add(name);
            numbers.add(number);
        }

        public List<String> getNames() {
            return names;
        }

        public List<Integer> getNumbers() {
            return numbers;
        }

        public String toString(){
            StringBuilder result = new StringBuilder();
            for(int x = 0; x < names.size(); x++){
                result.append(names.get(x)).append(";").append(numbers.get(x)).append(";");
            }
            return result.toString();
        }

        public int getNet(){
            return numbers.get(0) - numbers.get(1);
        }

        public int getUndecided(){
            int voted = 0;
            for(Integer num : numbers){
                voted += num;
            }
            return 100 - voted;
        }

        public String getBestOfRest(){
            if(numbers.size() == 2){
                return "";
            }else{
                return names.get(2) + " " + numbers.get(2);
            }
        }

    }

    @Override
    public boolean equals(Object o){
        if(o == this)
            return true;
        if(o instanceof Poll){
            Poll poll = (Poll) o;
            return (ID.equals(poll.ID));
        }
        return false;
    }

    public String toString(){
        return date.toString() + " " + state + " " + results;
    }

    public Integer getDistrict(){
        return district;
    }

    public Integer generateDistrict(){
        if(getPollType().equals(Type.HOUSE)){
            return Integer.parseInt(getState().substring(getState().indexOf('-') + 1));
        }else if(getPollType().equals(Type.SENATE) && getState().contains(".") && getState().length() > getState().lastIndexOf('.') + 1){
            return Integer.parseInt(getState().substring(getState().indexOf('.') + 2));
        }
        return null;
    }

    public void setDistrict(Integer district) {
        this.district = district;
    }

    public String removeDistrictNumber(){
        if(getPollType().equals(Type.HOUSE)){
            return getState().substring(0, getState().indexOf('-'));
        }else if(getPollType().equals(Type.SENATE) && getState().contains(".") && getState().length() > getState().lastIndexOf('.') + 1){
            return getState().substring(0, getState().lastIndexOf('.') + 1);
        }
        return getState();
    }

    public void unifyStates(){
        for(State s : MainController.states){
            if(getState().equals(s.getTwoLetters())){
                state = s.getShortName();
            }
        }
    }

    public String getDateConducted() {
        return dateConducted;
    }

    public String getPollster() {
        return pollster;
    }

    public int getSampleSize() {
        return sampleSize;
    }

    public String getSampleType() {
        return sampleType;
    }

    public void setDateConducted(String dateConducted) {
        this.dateConducted = dateConducted;
    }

    public void setPollster(String pollster) {
        this.pollster = pollster;
    }

    public void setSampleSize(int sampleSize) {
        this.sampleSize = sampleSize;
    }

    public void setSampleType(String sampleType) {
        this.sampleType = sampleType;
    }

    public Type getPollType() {
        return pollType;
    }

    public void setPollType(Type pollType) {
        this.pollType = pollType;
    }

    public Result getResults() {
        return results;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public boolean isHighlight() {
        return highlight;
    }

    public void setHighlight(boolean highlight) {
        this.highlight = highlight;
    }

    public boolean is_new() {
        return _new;
    }

    public void set_new(boolean _new) {
        this._new = _new;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public boolean isCustom() {
        return custom;
    }

    public void setCustom(boolean custom) {
        this.custom = custom;
    }
}
