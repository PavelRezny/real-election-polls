package code;

import java.util.ArrayList;
import java.util.List;

public class District {

    private Long ID;
    private boolean active = true;
    // Presidential district starts at 1
    // Gubernial district starts at 10
    // Senate districts starts at 100
    // House districts starts at 1000
    private final int number;

    private Long stateID;

    private final List<ElectionResult> result = new ArrayList<>();

    public District(int number){
        this.number = number;
    }

    public Long getStateID() {
        return stateID;
    }

    public void setStateID(Long stateID) {
        this.stateID = stateID;
    }

    public int getNumber() {
        return number;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public List<ElectionResult> getResult() {
        return result;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
}
