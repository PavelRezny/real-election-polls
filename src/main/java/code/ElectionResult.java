package code;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class ElectionResult {

    private Long ID;

    private final LocalDate date;
    private final List<ResultPerson> names = new ArrayList<>();
    private final List<ResultNumber> numbers = new ArrayList<>();

    private Long ownerID;

    public ElectionResult(LocalDate date){
        this.date = date;
    }

    public static class ResultPerson{

        private Long ID;
        private Long electionResultID;
        private Long personID;

        private final Person person;

        public ResultPerson(Person person){
            this.person = person;

            personID = person.getID();
        }

        public Long getID() {
            return ID;
        }

        public void setID(Long ID) {
            this.ID = ID;
        }

        public Long getElectionResultID() {
            return electionResultID;
        }

        public void setElectionResultID(Long electionResultID) {
            this.electionResultID = electionResultID;
        }

        public Long getPersonID() {
            return personID;
        }

        public void setPersonID(Long personID) {
            this.personID = personID;
        }

        public Person getPerson() {
            return person;
        }
    }

    @Override
    public String toString(){
        return date.toString();
    }

    public static class ResultNumber {

        private Long ID;
        private Long electionResultID;

        private final int votes;

        public ResultNumber(int votes){
            this.votes = votes;
        }

        public Long getElectionResultID() {
            return electionResultID;
        }

        public void setElectionResultID(Long electionResultID) {
            this.electionResultID = electionResultID;
        }

        public void setID(Long ID) {
            this.ID = ID;
        }

        public Long getID() {
            return ID;
        }

        public int getVotes() {
            return votes;
        }
    }

    public Long getOwnerID() {
        return ownerID;
    }

    public void setOwnerID(Long ownerID) {
        this.ownerID = ownerID;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public LocalDate getDate() {
        return date;
    }

    public List<ResultPerson> getNames() {
        return names;
    }

    public List<ResultNumber> getNumbers() {
        return numbers;
    }
}
