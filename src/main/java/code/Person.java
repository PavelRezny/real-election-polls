package code;

public class Person {

    private Long ID;

    private final String name;
    private final Party party;

    public Person(String name, Party party){
        this.name = name;
        this.party = party;
    }

    @Override
    public String toString(){
        return name + " (" + party.getLetter() + ")";
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }

    public String getName() {
        return name;
    }

    public Party getParty() {
        return party;
    }
}
