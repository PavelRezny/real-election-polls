package code;

import java.util.ArrayList;
import java.util.List;

public class State {

    private Long ID;
    private String fullName, shortName, twoLetters;

    private final List<District> houseDistricts = new ArrayList<>();
    private final List<District> senateDistricts = new ArrayList<>();
    private District presidentialDistrict;
    private District gubernialDistrict;

    public State(String fullName, String shortName, String twoLetters) {
        this.fullName = fullName;
        this.shortName = shortName;
        this.twoLetters = twoLetters;
    }

    public void addHouseDistrict(){
        int number = (houseDistricts.size() == 0) ? 1 : (houseDistricts.get(houseDistricts.size() - 1).getNumber() + 1);
        houseDistricts.add(new District(number));
    }

    public void addSenateDistrict(){
        int number = (senateDistricts.size() == 0) ? 1 : (senateDistricts.get(senateDistricts.size() - 1).getNumber() + 1);
        senateDistricts.add(new District(number));
    }

    public int getNumberOfActiveDistricts(){
        int result = 0;
        for(District d : houseDistricts){
            if(d.isActive())
                result++;
        }
        return result;
    }

    @Override
    public String toString(){
        return fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getTwoLetters() {
        return twoLetters;
    }

    public void setTwoLetters(String twoLetters) {
        this.twoLetters = twoLetters;
    }

    public List<District> getHouseDistricts() {
        return houseDistricts;
    }

    public List<District> getSenateDistricts() {
        return senateDistricts;
    }

    public District getPresidentialDistrict() {
        return presidentialDistrict;
    }

    public void setPresidentialDistrict(District presidentialDistrict) {
        this.presidentialDistrict = presidentialDistrict;
    }

    public District getGubernialDistrict() {
        return gubernialDistrict;
    }

    public void setGubernialDistrict(District gubernialDistrict) {
        this.gubernialDistrict = gubernialDistrict;
    }

    public Long getID() {
        return ID;
    }

    public void setID(Long ID) {
        this.ID = ID;
    }
}