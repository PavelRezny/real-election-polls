package code;

import data.PollDao;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URLDecoder;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class DownloadData {

    // TODO: Fix states and districts

    public static void getContent(PollDao pollDao){
        try{
            Connection connect = Jsoup.connect("https://projects.fivethirtyeight.com/polls/");
            connect.request().followRedirects(false);
            URI u = new URI("https://projects.fivethirtyeight.com/polls/");
            Document document = connect.url(new URI(u.getScheme(), u.getUserInfo(), u.getHost(), u.getPort(), URLDecoder.decode(u.getPath(), "UTF-8"), u.getQuery(), u.getFragment()).toURL()).get();

            //File in = new File("C:\\Users\\pavre\\OneDrive\\Plocha\\Latest Polls _ FiveThirtyEight.html");
            //document = Jsoup.parse(in, "UTF-8");

            Elements elements = document.select("[class=day-container]");
            for(Element e : elements){
                Elements day = e.select("[class=day]");
                LocalDateTime date;
                if(day.isEmpty()){
                    Elements dayHidden = e.select("[class=day hidden-date]");
                    String[] parts = dayHidden.attr("data-date").split("-");
                    date = LocalDateTime.of(Integer.parseInt(parts[0]) , Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), LocalDateTime.now().getHour(), LocalDateTime.now().getMinute());
                }else{
                    String[] parts = day.attr("data-date").split("-");
                    date = LocalDateTime.of(Integer.parseInt(parts[0]), Integer.parseInt(parts[1]), Integer.parseInt(parts[2]), LocalDateTime.now().getHour(), LocalDateTime.now().getMinute());
                }
                Elements table = e.select("table[class=polls-table]");
                Elements body = table.select("tbody");
                Elements rows = body.get(0).select("tr");
                for(int x = 0; x < rows.size(); x++){
                    if(rows.get(x).getElementsByClass("value hide-mobile").size() > 0){
                        Poll p = new Poll();
                        p.set_new(true);
                        p.setDate(date);
                        Elements name = rows.get(x).select("div.pollster-container > a");
                        p.setPollster(name.get(0).text());
                        p.setUrl(name.get(0).attr("href"));
                        p.setState(rows.get(x).getElementsByClass("race hide-mobile").text());
                        p.setPollType(Type.getType(rows.get(x).getElementsByClass("visible-row").attr("data-type")));
                        p.setDateConducted(rows.get(x).getElementsByClass("date-wrapper").text());
                        if(!(rows.get(x).getElementsByClass("sample hide-mobile").text()).equals("")){
                            p.setSampleSize(Integer.parseInt((rows.get(x).getElementsByClass("sample hide-mobile").text()).replace(",", "")));
                        }
                        p.setSampleType(rows.get(x).getElementsByClass("sample-type hide-mobile").text());
                        if(rows.get(x).getElementsByClass("see-more").text().equals("More")){
                            p.getResults().add(rows.get(x).getElementsByClass("answer first hide-mobile").text(), Integer.parseInt((rows.get(x).getElementsByClass("value hide-mobile").text()).replace("%", "")));
                            int more = 1;
                            while(x + more < rows.size() && !rows.get(x + more).getElementsByClass("expandable-row").text().isBlank()){
                                if(!rows.get(x + more).getElementsByClass("value hide-mobile expandable bottom").isEmpty()){
                                    p.getResults().add(rows.get(x + more).getElementsByClass("first answer hide-mobile expandable bottom").text(), Integer.parseInt((rows.get(x + more).getElementsByClass("value hide-mobile expandable bottom").text()).replace("%", "")));
                                }else{
                                    p.getResults().add(rows.get(x + more).getElementsByClass("first answer hide-mobile expandable not-bottom").text(), Integer.parseInt((rows.get(x + more).getElementsByClass("value hide-mobile expandable not-bottom").text()).replace("%", "")));
                                }
                                more += 1;
                            }
                        }else{
                            p.getResults().add(rows.get(x).getElementsByClass("answer first hide-mobile").text(), Integer.parseInt((rows.get(x).getElementsByClass("value hide-mobile").get(0).text()).replace("%", "")));
                            p.getResults().add(rows.get(x).getElementsByClass("answer hide-mobile").text(), Integer.parseInt((rows.get(x).getElementsByClass("value hide-mobile").get(1).text()).replace("%", "")));
                        }

                        fixGeorgiaSenate(p);
                        p.setDistrict(p.generateDistrict());
                        if(!checkForDuplicity(p, pollDao))
                            pollDao.create(p);
                    }
                }
            }
        }catch (IOException | URISyntaxException e ){
            e.printStackTrace();
        }
    }

    public static void fixGeorgiaSenate(Poll p){
        if(p.getPollType().equals(Type.SENATE) && p.getState().contains("Ga.") && (p.getResults().getNames().contains("Warnock") || p.getResults().getNames().contains("Lieberman"))){
            p.setState("Ga. 1");
        }else if(p.getPollType().equals(Type.SENATE) && p.getState().contains("Ga.") && p.getResults().getNames().contains("Perdue")){
            p.setState("Ga. 2");
        }
    }

    public static boolean checkForDuplicity(Poll poll, PollDao pollDao){
        List<Poll> duplicities = pollDao.find(Collections.singletonList(null));
        duplicities.removeIf(p -> !p.getPollster().equals(poll.getPollster()));
        duplicities.removeIf(p -> !p.getDateConducted().equals(poll.getDateConducted()));
        duplicities.removeIf(p -> !p.getSampleType().equals(poll.getSampleType()));
        duplicities.removeIf(p -> p.getSampleSize() != poll.getSampleSize());
        duplicities.removeIf(p -> !p.getPollType().equals(poll.getPollType()));
        duplicities.removeIf(p -> !p.getState().equals(poll.getState()));
        duplicities.removeIf(p -> !p.getUrl().equals(poll.getUrl()));
        duplicities.removeIf(p -> !p.getResults().toString().equals(poll.getResults().toString()));
        return (duplicities.size() > 0);
    }

}
