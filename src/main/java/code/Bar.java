package code;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.scene.text.Text;

public class Bar extends VBox {

    private Text text;
    private Rectangle rBar1, rBar2, rDivider;

    private int leftNum, rightNum;
    private String leftText, rightText;

    public Bar(int leftNum, int rightNum, String leftText, String rightText) {
        this.leftNum = leftNum;
        this.rightNum = rightNum;
        this.leftText = leftText;
        this.rightText = rightText;

        setNodes();
        setText();
        setRectangles();
    }

    public void setNodes(){
        text = new Text();
        text.setStyle("-fx-font: 25 verdana");

        rBar1 = new Rectangle(400, 40);
        rBar1.setFill(Color.BLUE);
        rBar1.setStroke(Color.BLACK);

        rDivider = new Rectangle(10, 40);
        rDivider.setFill(Color.WHITE);
        rDivider.setStroke(Color.BLACK);

        rBar2 = new Rectangle(400, 40);
        rBar2.setFill(Color.RED);
        rBar2.setStroke(Color.BLACK);

        VBox vBoxText = new VBox();
        vBoxText.setAlignment(Pos.CENTER);
        vBoxText.getChildren().add(text);

        GridPane gpBar = new GridPane();
        gpBar.setAlignment(Pos.CENTER);
        gpBar.setPadding(new Insets(10, 10, 10, 10));

        gpBar.add(rBar1, 0, 0);
        gpBar.add(rDivider, 1, 0);
        gpBar.add(rBar2, 2, 0);

        getChildren().addAll(vBoxText, gpBar);
    }

    public void setText() {
        text.setText(leftText + " " + leftNum + " - " + rightNum + " " + rightText);
    }

    // TODO: Should run on windows resize to change the size of the bar
    // TODO: Maybe change the height as well

    public void setRectangles() {
        double value = ((double)(leftNum + rightNum) == 0.0) ? 0.5 : leftNum / (double)(leftNum + rightNum);
        double width = (getWidth() == 0) ? 800 : getWidth() * 0.8;
        rBar1.setWidth(width - (width * (1 - value)));
        rBar2.setWidth(width - (width * value));
        rDivider.setWidth(width * 0.0125);
    }

    public void changeNumbers(int left, int right){
        leftNum = left;
        rightNum = right;
        setText();
        setRectangles();
    }

}
