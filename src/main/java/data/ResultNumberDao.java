package data;

import code.ElectionResult;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class ResultNumberDao {

    private final DataSource dataSource;

    public ResultNumberDao(DataSource dataSource) {
        this.dataSource = dataSource;
        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.ELECTION_NUMBERS (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "VOTES INT NOT NULL," +
                    "RESULT_ID BIGINT" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create ELECTION_NUMBERS table", e);
        }
    }

    public void create(ElectionResult.ResultNumber numbers) {
        if (numbers.getID() != null) {
            throw new IllegalArgumentException("Numbers already has ID: " + numbers);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO ELECTION_NUMBERS (VOTES, RESULT_ID) VALUES (?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setInt(1, numbers.getVotes());
            if(numbers.getElectionResultID() == null)
                st.setNull(2, Types.BIGINT);
            else
                st.setLong(2, numbers.getElectionResultID());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    numbers.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for numbers: " + numbers);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store number " + numbers, e);
        }
    }

    public List<ElectionResult.ResultNumber> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, VOTES, RESULT_ID FROM ELECTION_NUMBERS")) {

            List<ElectionResult.ResultNumber> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    ElectionResult.ResultNumber number = new ElectionResult.ResultNumber(
                            rs.getInt("VOTES"));
                    number.setID(rs.getLong("ID"));
                    number.setElectionResultID(rs.getLong("RESULT_ID"));

                    result.add(number);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all numbers", ex);
        }
    }

    public List<ElectionResult.ResultNumber> findByID(Long ID) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ELECTION_NUMBERS.ID, VOTES, RESULT_ID FROM ELECTION_NUMBERS WHERE RESULT_ID = ?")) {

            st.setLong(1, ID);

            List<ElectionResult.ResultNumber> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    ElectionResult.ResultNumber number = new ElectionResult.ResultNumber(
                            rs.getInt("VOTES"));
                    number.setID(rs.getLong("ID"));
                    number.setElectionResultID(rs.getLong("RESULT_ID"));

                    result.add(number);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all results", ex);
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.ELECTION_NUMBERS");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop ELECTION_NUMBERS table", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "ELECTION_NUMBERS")) {
            createTable();
        }
    }

}
