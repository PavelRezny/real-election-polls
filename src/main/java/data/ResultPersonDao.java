package data;

import code.ElectionResult;
import code.Person;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class ResultPersonDao {

    private final DataSource dataSource;

    private PersonDao personDao;

    public ResultPersonDao(DataSource dataSource, PersonDao personDao) {
        this.dataSource = dataSource;
        this.personDao = personDao;

        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.RESULT_PEOPLE (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "RESULT_ID BIGINT," +
                    "PERSON_ID BIGINT" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create RESULT_PEOPLE table", e);
        }
    }

    public void create(ElectionResult.ResultPerson person) {
        if (person.getID() != null) {
            throw new IllegalArgumentException("Person already has ID: " + person);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO RESULT_PEOPLE (RESULT_ID, PERSON_ID) VALUES (?, ?)",
                     RETURN_GENERATED_KEYS)) {

            if(person.getElectionResultID() == null)
                st.setNull(1, Types.BIGINT);
            else
                st.setLong(1, person.getElectionResultID());
            if(person.getPersonID() == null)
                st.setNull(2, Types.BIGINT);
            else
                st.setLong(2, person.getPersonID());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    person.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for person: " + person);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store person " + person, e);
        }
    }

    public List<ElectionResult.ResultPerson> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, RESULT_ID, PERSON_ID FROM RESULT_PEOPLE")) {

            List<ElectionResult.ResultPerson> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    Person p = personDao.findByID(rs.getLong("PERSON_ID"));
                    ElectionResult.ResultPerson person = new ElectionResult.ResultPerson(p);
                    person.setID(rs.getLong("ID"));
                    person.setPersonID(p.getID());
                    person.setElectionResultID(rs.getLong("RESULT_ID"));

                    result.add(person);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all people", ex);
        }
    }

    public List<ElectionResult.ResultPerson> findByID(Long ID) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, RESULT_ID, PERSON_ID FROM RESULT_PEOPLE WHERE RESULT_ID = ?")) {

            st.setLong(1, ID);

            List<ElectionResult.ResultPerson> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    Person p = personDao.findByID(rs.getLong("PERSON_ID"));
                    ElectionResult.ResultPerson person = new ElectionResult.ResultPerson(p);
                    person.setID(rs.getLong("ID"));
                    person.setPersonID(p.getID());
                    person.setElectionResultID(rs.getLong("RESULT_ID"));

                    result.add(person);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all people", ex);
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.RESULT_PEOPLE");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop RESULT_PEOPLE table", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "RESULT_PEOPLE")) {
            createTable();
        }
    }

}
