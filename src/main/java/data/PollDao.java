package data;

import code.Poll;
import code.State;
import code.Type;

import javax.sql.DataSource;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.time.LocalDate;
import java.util.*;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class PollDao {

    private final DataSource dataSource;

    public PollDao(DataSource dataSource) {
        this.dataSource = dataSource;
        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.POLL (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "DATE TIMESTAMP NOT NULL," +
                    "DATE_CONDUCTED VARCHAR(50) NOT NULL," +
                    "POLLSTER VARCHAR(500) NOT NULL," +
                    "TYPE VARCHAR(500) NOT NULL," +
                    "SAMPLE_TYPE VARCHAR(100)," +
                    "SAMPLE_SIZE INT," +
                    "STATE VARCHAR(300) NOT NULL," +
                    "DISTRICT INT," +
                    "URL VARCHAR(10000)," +
                    "BANNED BOOLEAN NOT NULL," +
                    "RESULT_NAMES VARCHAR(5000) NOT NULL," +
                    "RESULT_VALUES VARCHAR(5000) NOT NULL," +
                    "CUSTOM BOOLEAN" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create POLLS table", e);
        }
    }

    public void create(Poll poll) {
        if (poll.getID() != null) {
            throw new IllegalArgumentException("Poll already has ID: " + poll);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO POLL (DATE, DATE_CONDUCTED, POLLSTER, TYPE, SAMPLE_TYPE, SAMPLE_SIZE, STATE, DISTRICT, URL, BANNED, RESULT_NAMES, RESULT_VALUES, CUSTOM) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setTimestamp(1, Timestamp.valueOf(poll.getDate()));
            st.setString(2, poll.getDateConducted());
            st.setString(3, poll.getPollster());
            st.setString(4, poll.getPollType().name());
            st.setString(5, poll.getSampleType());
            st.setInt(6, poll.getSampleSize());
            st.setString(7, poll.getState());
            if(poll.getDistrict() == null){
                st.setNull(8, Types.INTEGER);
            }else{
                st.setInt(8,poll.getDistrict());
            }
            st.setString(9, poll.getUrl());
            st.setBoolean(10, poll.isBanned());
            st.setString(11, poll.getResults().getNames().toString());
            st.setString(12, poll.getResults().getNumbers().toString());
            st.setBoolean(13, poll.isCustom());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    poll.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for poll: " + poll);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store poll " + poll, e);
        }
    }

    public enum SQLString{

        SQL_ALL("SELECT ID, DATE, DATE_CONDUCTED, POLLSTER, TYPE, SAMPLE_TYPE, SAMPLE_SIZE, STATE, DISTRICT, URL, BANNED, RESULT_NAMES, RESULT_VALUES, CUSTOM FROM POLL"),
        SQL_POLLSTER(" POLLSTER = ?"),
        SQL_TYPE(" TYPE = ?"),
        SQL_DATE(" (DATE > ? AND DATE <= ?)"),
        SQL_STATE(" STATE = ?"),
        SQL_DISTRICT(" DISTRICT = ? "),
        SQL_BANNED_POLLS(" BANNED = ?");

        private final String string;

        SQLString(String string){
            this.string = string;
        }

        public String getString(){
            return string;
        }

    }

    public List<Poll> find(List<List<Object>> input) {
        // Remove all null values from list
        List<List<Object>> listWithoutNull = new ArrayList<>();
        for(List<Object> list : input){
            if(list != null)
                listWithoutNull.add(list);
        }
        // Build the sql query
        StringBuilder sql = new StringBuilder();
        if(listWithoutNull.size() > 0){
            Iterator<List<Object>> iterator = listWithoutNull.iterator();
            while (iterator.hasNext()) {
                sql.append(((SQLString) iterator.next().get(0)).getString());
                if(iterator.hasNext())
                    sql.append(" AND ");
            }
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     SQLString.SQL_ALL.getString() + ((listWithoutNull.size() == 0) ? "" : " WHERE " + sql) + " ORDER BY DATE DESC")) {

            if(listWithoutNull.size() > 0){
                int currentParameterIndex = 1;
                for(List<Object> list : listWithoutNull){
                    if(list != null && list.size() > 1){
                        if(list.get(0).equals(SQLString.SQL_POLLSTER)){
                            st.setString(currentParameterIndex, (String) list.get(1));
                            currentParameterIndex++;
                        }else if(list.get(0) == SQLString.SQL_TYPE){
                            st.setString(currentParameterIndex, ((Type) list.get(1)).name());
                            currentParameterIndex++;
                        }else if(list.get(0) == SQLString.SQL_DATE){
                            st.setDate(currentParameterIndex, Date.valueOf((LocalDate) list.get(1)));
                            st.setDate(currentParameterIndex + 1, Date.valueOf((LocalDate) list.get(2)));
                            currentParameterIndex += 2;
                        }else if(list.get(0) == SQLString.SQL_STATE){
                            st.setString(currentParameterIndex, ((State)list.get(1)).getShortName());
                            currentParameterIndex++;
                        }else if(list.get(0) == SQLString.SQL_DISTRICT){
                            st.setInt(currentParameterIndex, (Integer)list.get(1));
                            currentParameterIndex++;
                        }else if(list.get(0) == SQLString.SQL_BANNED_POLLS){
                            st.setBoolean(currentParameterIndex, (Boolean) list.get(1));
                            currentParameterIndex++;
                        }
                    }
                }
            }

            List<Poll> polls = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    Poll poll = new Poll(
                            rs.getTimestamp("DATE").toLocalDateTime(),
                            rs.getString("DATE_CONDUCTED"),
                            rs.getString("POLLSTER"),
                            Type.valueOf(rs.getString("TYPE")),
                            rs.getString("SAMPLE_TYPE"),
                            rs.getInt("SAMPLE_SIZE"),
                            rs.getString("STATE"),
                            rs.getString("URL"),
                            rs.getBoolean("CUSTOM")
                    );
                    poll.setID(rs.getLong("ID"));
                    poll.setBanned(rs.getBoolean("BANNED"));
                    poll.setDistrict(rs.getInt("DISTRICT"));

                    String names = rs.getString("RESULT_NAMES").replaceAll("\\s+","");
                    names = names.substring(1, names.length() - 1);
                    List<String> resultNames = new ArrayList<>(Arrays.asList(names.split(",")));
                    String values = rs.getString("RESULT_VALUES").replaceAll("\\s+","");
                    values = values.substring(1, values.length() - 1);
                    List<String> resultValues = new ArrayList<>(Arrays.asList(values.split(",")));
                    for(int x = 0; x < resultNames.size(); x++){
                        poll.getResults().add(resultNames.get(x), Integer.parseInt(resultValues.get(x)));
                    }

                    polls.add(poll);
                }
            }
            return polls;
        } catch (SQLException e) {
            throw new RuntimeException("Failed to load all polls", e);
        }
    }

    public void delete(Poll poll) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "DELETE FROM POLL WHERE ID = ?")) {

            st.setLong(1, poll.getID());
            int num = st.executeUpdate();

            if(num == 0)
                throw new RuntimeException("Failed to delete non-existing poll: " + poll);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to delete poll " + poll, e);
        }
    }

    public void update(Poll poll) {
        if (poll.getID() == null) {
            throw new IllegalArgumentException("Poll has null ID");
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement("UPDATE POLL SET DATE = ?, DATE_CONDUCTED = ?," +
                     " POLLSTER = ?, TYPE = ?, SAMPLE_TYPE = ?, SAMPLE_SIZE = ?, STATE = ?, DISTRICT = ?, URL = ?, BANNED = ?, RESULT_NAMES = ?, RESULT_VALUES = ?, CUSTOM = ? WHERE ID = ?")) {

            st.setTimestamp(1, Timestamp.valueOf(poll.getDate()));
            st.setString(2, poll.getDateConducted());
            st.setString(3, poll.getPollster());
            st.setString(4, poll.getPollType().name());
            st.setString(5, poll.getSampleType());
            st.setInt(6, poll.getSampleSize());
            st.setString(7, poll.getState());
            st.setInt(8, poll.getDistrict());
            st.setString(9, poll.getUrl());
            st.setBoolean(10, poll.isBanned());
            st.setString(11, poll.getResults().getNames().toString());
            st.setString(12, poll.getResults().getNumbers().toString());
            st.setBoolean(13, poll.isCustom());
            st.setLong(14, poll.getID());
            st.executeUpdate();

            if (st.executeUpdate() == 0) {
                throw new RuntimeException("Failed to update non-existing poll: " + poll);
            }
        } catch (SQLException x) {
            throw new RuntimeException("Failed to update poll: " + poll, x);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "POLL")) {
            createTable();
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.POLL");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop POLL table", ex);
        }
    }

}
