package data;

import code.District;
import code.ElectionResult;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class DistrictDao {

    private final DataSource dataSource;

    private final ElectionResultDao electionResultDao;

    public DistrictDao(DataSource dataSource, ElectionResultDao electionResultDao) {
        this.dataSource = dataSource;
        this.electionResultDao = electionResultDao;

        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.DISTRICT (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "NUMBER INT NOT NULL," +
                    "ACTIVE BOOLEAN NOT NULL," +
                    "STATE_ID BIGINT REFERENCES APP.STATE(ID)" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create DISTRICT table", e);
        }
    }

    public void create(District district) {
        if (district.getID() != null) {
            throw new IllegalArgumentException("District already has ID: " + district);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO DISTRICT (NUMBER, ACTIVE, STATE_ID) VALUES (?, ?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setInt(1, district.getNumber());
            st.setBoolean(2, district.isActive());
            if(district.getStateID() == null)
                st.setNull(3, Types.BIGINT);
            else
                st.setLong(3, district.getStateID());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    district.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for district: " + district);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store district " + district, e);
        }
    }

    public List<District> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT DISTRICT.ID, NUMBER, ACTIVE, STATE_ID FROM DISTRICT")) {

            List<District> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    District district = new District(
                            rs.getInt("NUMBER"));
                    district.setActive(rs.getBoolean("ACTIVE"));
                    district.setStateID(rs.getLong("STATE_ID"));
                    district.setID(rs.getLong("ID"));

                    List<ElectionResult> e = electionResultDao.findByID(rs.getLong("ID"));
                    district.getResult().addAll(e);

                    result.add(district);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all districts", ex);
        }
    }

    // Identifier can be 1, 10, 100, 1000, and it sets the district numbers -> identifier 100 means return districts 100 - 999
    public List<District> findByStateID(Long ID, int identifier) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT DISTRICT.ID, NUMBER, ACTIVE, STATE_ID FROM DISTRICT WHERE STATE_ID = ? AND NUMBER >= ? AND NUMBER < ?")) {

            st.setLong(1, ID);
            st.setInt(2, identifier);
            st.setInt(3, identifier * 10);

            List<District> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    District district = new District(
                            rs.getInt("NUMBER"));
                    district.setActive(rs.getBoolean("ACTIVE"));
                    district.setStateID(rs.getLong("STATE_ID"));
                    district.setID(rs.getLong("ID"));

                    List<ElectionResult> e = electionResultDao.findByID(rs.getLong("ID"));
                    district.getResult().addAll(e);

                    result.add(district);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all districts", ex);
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.DISTRICT");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop DISTRICT table", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "DISTRICT")) {
            createTable();
        }
    }

}
