package data;

import code.State;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class StateDao {

    private final DataSource dataSource;
    private final DistrictDao districtDao;

    public StateDao(DataSource dataSource, DistrictDao districtDao) {
        this.dataSource = dataSource;
        this.districtDao = districtDao;

        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.STATE (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "FULL_NAME VARCHAR(100) NOT NULL," +
                    "SHORT_NAME VARCHAR(100) NOT NULL," +
                    "TWO_LETTERS VARCHAR(10) NOT NULL" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create STATE table", e);
        }
    }

    public void create(State state) {
        if (state.getID() != null) {
            throw new IllegalArgumentException("State already has ID: " + state);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO STATE (FULL_NAME, SHORT_NAME, TWO_LETTERS) VALUES (?, ?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setString(1, state.getFullName());
            st.setString(2, state.getShortName());
            st.setString(3, state.getTwoLetters());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    state.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for state: " + state);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store state " + state, e);
        }
    }

    public List<State> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, FULL_NAME, SHORT_NAME, TWO_LETTERS FROM STATE")) {

            List<State> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    State state = new State(
                            rs.getString("FULL_NAME"),
                            rs.getString("SHORT_NAME"),
                            rs.getString("TWO_LETTERS"));
                    state.setID(rs.getLong("ID"));

                    state.getHouseDistricts().addAll(districtDao.findByStateID(state.getID(), 1000));
                    state.getSenateDistricts().addAll(districtDao.findByStateID(state.getID(), 100));
                    if(districtDao.findByStateID(state.getID(), 1).size() != 0)
                        state.setPresidentialDistrict(districtDao.findByStateID(state.getID(), 1).get(0));
                    if(districtDao.findByStateID(state.getID(), 10).size() != 0)
                        state.setGubernialDistrict(districtDao.findByStateID(state.getID(), 10).get(0));

                    result.add(state);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all states", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "STATE")) {
            createTable();
        }
    }

}
