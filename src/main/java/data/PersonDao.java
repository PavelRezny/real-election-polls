package data;

import code.Party;
import code.Person;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class PersonDao {

    private final DataSource dataSource;

    public PersonDao(DataSource dataSource) {
        this.dataSource = dataSource;
        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.PERSON (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "NAME VARCHAR(100) NOT NULL," +
                    "PARTY_LETTER VARCHAR(10) NOT NULL" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create PERSON table", e);
        }
    }

    public void create(Person person) {
        if (person.getID() != null) {
            throw new IllegalArgumentException("Person already has ID: " + person);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO PERSON (NAME, PARTY_LETTER) VALUES (?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setString(1, person.getName());
            st.setString(2, person.getParty().getLetter());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    person.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for person: " + person);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store parson " + person, e);
        }
    }

    public List<Person> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, NAME, PARTY_LETTER FROM PERSON")) {

            List<Person> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    String partyLetter = rs.getString("PARTY_LETTER");
                    Party p;
                    if(partyLetter.equals("R"))
                        p = Party.R;
                    else if(partyLetter.equals("D"))
                        p = Party.D;
                    else if(partyLetter.equals("I"))
                        p = Party.I;
                    else if(partyLetter.equals("G"))
                        p = Party.G;
                    else if(partyLetter.equals("L"))
                        p = Party.L;
                    else
                        p = Party.O;
                    Person person = new Person(
                            rs.getString("NAME"),
                            p);
                    person.setID(rs.getLong("ID"));

                    result.add(person);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all persons", ex);
        }
    }

    public Person findByID(Long ID) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT PERSON.ID, NAME, PARTY_LETTER FROM PERSON WHERE ID = ?")) {

            st.setLong(1, ID);

            Person result = null;
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    String partyLetter = rs.getString("PARTY_LETTER");
                    Party p;
                    if(partyLetter.equals("R"))
                        p = Party.R;
                    else if(partyLetter.equals("D"))
                        p = Party.D;
                    else if(partyLetter.equals("I"))
                        p = Party.I;
                    else if(partyLetter.equals("G"))
                        p = Party.G;
                    else if(partyLetter.equals("L"))
                        p = Party.L;
                    else
                        p = Party.O;
                    result = new Person(
                            rs.getString("NAME"),
                            p);
                    result.setID(rs.getLong("ID"));
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all persons", ex);
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.PERSON");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop PERSON table", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "PERSON")) {
            createTable();
        }
    }

}
