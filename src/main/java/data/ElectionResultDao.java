package data;

import code.ElectionResult;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class ElectionResultDao {

    private final DataSource dataSource;

    private final ResultPersonDao resultPersonDao;
    private final ResultNumberDao resultNumberDao;

    public ElectionResultDao(DataSource dataSource, ResultPersonDao resultPersonDao, ResultNumberDao resultNumberDao) {
        this.dataSource = dataSource;
        this.resultPersonDao = resultPersonDao;
        this.resultNumberDao = resultNumberDao;

        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.RESULTS (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "DATE TIMESTAMP NOT NULL," +
                    "OWNER_ID BIGINT NOT NULL" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create RESULTS table", e);
        }
    }

    public void create(ElectionResult result) {
        if (result.getID() != null) {
            throw new IllegalArgumentException("Result already has ID: " + result);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO RESULTS (DATE, OWNER_ID) VALUES (?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setTimestamp(1, Timestamp.valueOf(result.getDate().atStartOfDay()));
            if(result.getOwnerID() == null)
                st.setNull(2, Types.BIGINT);
            else
                st.setLong(2, result.getOwnerID());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    result.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for result: " + result);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store result " + result, e);
        }
    }

    public List<ElectionResult> findAll() {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, DATE, OWNER_ID FROM RESULTS")) {

            List<ElectionResult> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    ElectionResult electionResult = new ElectionResult(
                            rs.getTimestamp("DATE").toLocalDateTime().toLocalDate());
                    electionResult.getNames().addAll(resultPersonDao.findByID(rs.getLong("ID")));
                    electionResult.getNumbers().addAll(resultNumberDao.findByID(rs.getLong("ID")));
                    electionResult.setID(rs.getLong("ID"));
                    electionResult.setOwnerID(rs.getLong("OWNER_ID"));

                    result.add(electionResult);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all results", ex);
        }
    }

    public List<ElectionResult> findByID(Long ID) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT RESULTS.ID, DATE, OWNER_ID FROM RESULTS WHERE OWNER_ID = ?")) {

            st.setLong(1, ID);

            List<ElectionResult> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    ElectionResult electionResult = new ElectionResult(
                            rs.getTimestamp("DATE").toLocalDateTime().toLocalDate());
                    electionResult.getNames().addAll(resultPersonDao.findByID(rs.getLong("ID")));
                    electionResult.getNumbers().addAll(resultNumberDao.findByID(rs.getLong("ID")));
                    electionResult.setID(rs.getLong("ID"));
                    electionResult.setOwnerID(rs.getLong("OWNER_ID"));

                    result.add(electionResult);
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all results", ex);
        }
    }

    public void dropTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("DROP TABLE APP.RESULTS");
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to drop RESULTS table", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "RESULTS")) {
            createTable();
        }
    }

}
