package data;

import code.TimePeriod;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class TimePeriodDao {

    private final DataSource dataSource;

    public TimePeriodDao(DataSource dataSource) {
        this.dataSource = dataSource;
        initTable();
    }

    private void createTable() {
        try (var connection = dataSource.getConnection();
             var st = connection.createStatement()) {

            st.executeUpdate("CREATE TABLE APP.TIMEPERIOD (" +
                    "ID BIGINT PRIMARY KEY GENERATED ALWAYS AS IDENTITY," +
                    "DATE_FROM TIMESTAMP NOT NULL," +
                    "DATE_TO TIMESTAMP NOT NULL," +
                    "NAME VARCHAR(100) NOT NULL" +
                    ")");
        } catch (SQLException e) {
            throw new RuntimeException("Failed to create TIMEPERIOD table", e);
        }
    }

    public void create(TimePeriod timePeriod) {
        if (timePeriod.getID() != null) {
            throw new IllegalArgumentException("TimePeriod already has ID: " + timePeriod);
        }
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "INSERT INTO TIMEPERIOD (DATE_FROM, DATE_TO, NAME) VALUES (?, ?, ?)",
                     RETURN_GENERATED_KEYS)) {

            st.setTimestamp(1, Timestamp.valueOf(timePeriod.getFrom()));
            st.setTimestamp(2, Timestamp.valueOf(timePeriod.getTo()));
            st.setString(3, timePeriod.getName());
            st.executeUpdate();

            try (var rs = st.getGeneratedKeys()) {
                if (rs.next()) {
                    timePeriod.setID(rs.getLong(1));
                } else {
                    throw new RuntimeException("Failed to fetch generated key: no key returned for timePeriod: " + timePeriod);
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException("Failed to store timePeriod " + timePeriod, e);
        }
    }

    public void delete(TimePeriod timePeriod) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "DELETE FROM TIMEPERIOD WHERE ID = ?")) {

            st.setLong(1, timePeriod.getID());
            int num = st.executeUpdate();

            if(num == 0)
                throw new RuntimeException("Failed to delete non-existing timePeriod: " + timePeriod);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to delete timePeriod " + timePeriod, e);
        }
    }

    public List<TimePeriod> findAll(boolean two) {
        try (var connection = dataSource.getConnection();
             var st = connection.prepareStatement(
                     "SELECT ID, DATE_FROM, DATE_TO, NAME FROM TIMEPERIOD")) {

            List<TimePeriod> result = new ArrayList<>();
            try (var rs = st.executeQuery()) {
                while (rs.next()) {
                    TimePeriod timePeriod = new TimePeriod(
                            rs.getTimestamp("DATE_FROM").toLocalDateTime(),
                            rs.getTimestamp("DATE_TO").toLocalDateTime(),
                            rs.getString("NAME"));
                    timePeriod.setID(rs.getLong("ID"));
                    if(two && timePeriod.getName().isBlank()){
                        result.add(timePeriod);
                    }else if(!two && !timePeriod.getName().isBlank()){
                        result.add(timePeriod);
                    }
                }
            }
            return result;
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to load all timePeriods", ex);
        }
    }

    private boolean tableExits(String schema, String table) {
        try (var connection = dataSource.getConnection();
             var rs = connection.getMetaData().getTables(null, schema, table, null)) {
            return rs.next();
        } catch (SQLException ex) {
            throw new RuntimeException("Failed to detect if the table " + schema + "." + table + " exists", ex);
        }
    }

    public void initTable() {
        if (!tableExits("APP", "TIMEPERIOD")) {
            createTable();
        }
    }

}
